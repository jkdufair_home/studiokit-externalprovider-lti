using StudioKit.ExternalProvider.Lti.Models;
using System.Collections.Generic;

namespace StudioKit.ExternalProvider.Lti.Tests.EqualityComparers
{
	public class LtiLaunchEqualityComparer : IEqualityComparer<LtiLaunch>
	{
		public bool Equals(LtiLaunch x, LtiLaunch y)
		{
			if (x == y) return true;
			if (x is null) return false;
			if (y is null) return false;
			if (x.GetType() != y.GetType()) return false;
			return x.ExternalProviderId == y.ExternalProviderId &&
					string.Equals(x.CreatedById, y.CreatedById) &&
					string.Equals(x.ExternalId, y.ExternalId) &&
					string.Equals(x.Description, y.Description) &&
					string.Equals(x.Name, y.Name) &&
					string.Equals(x.Roles, y.Roles) &&
					string.Equals(x.RosterUrl, y.RosterUrl) &&
					string.Equals(x.GradesUrl, y.GradesUrl) &&
					string.Equals(x.LtiVersion, y.LtiVersion) &&
					string.Equals(x.ReturnUrl, y.ReturnUrl);
		}

		public int GetHashCode(LtiLaunch obj)
		{
			unchecked
			{
				var hashCode = obj.ExternalProviderId;
				hashCode = (hashCode * 397) ^ (obj.CreatedById != null ? obj.CreatedById.GetHashCode() : 0);
				hashCode = (hashCode * 397) ^ (obj.ExternalId != null ? obj.ExternalId.GetHashCode() : 0);
				hashCode = (hashCode * 397) ^ (obj.Description != null ? obj.Description.GetHashCode() : 0);
				hashCode = (hashCode * 397) ^ (obj.Name != null ? obj.Name.GetHashCode() : 0);
				hashCode = (hashCode * 397) ^ (obj.Roles != null ? obj.Roles.GetHashCode() : 0);
				hashCode = (hashCode * 397) ^ (obj.RosterUrl != null ? obj.RosterUrl.GetHashCode() : 0);
				hashCode = (hashCode * 397) ^ (obj.GradesUrl != null ? obj.GradesUrl.GetHashCode() : 0);
				hashCode = (hashCode * 397) ^ (obj.LtiVersion != null ? obj.LtiVersion.GetHashCode() : 0);
				hashCode = (hashCode * 397) ^ (obj.ReturnUrl != null ? obj.ReturnUrl.GetHashCode() : 0);
				return hashCode;
			}
		}
	}
}