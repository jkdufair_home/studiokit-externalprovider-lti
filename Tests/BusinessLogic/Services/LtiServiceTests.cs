﻿using Effort;
using FluentValidation;
using Microsoft.IdentityModel.Tokens;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using MSTestExtensions;
using Newtonsoft.Json;
using StudioKit.Data.Entity.Identity;
using StudioKit.Data.Entity.Identity.Models;
using StudioKit.ErrorHandling.Exceptions;
using StudioKit.ExternalProvider.Lti.BusinessLogic.Interfaces;
using StudioKit.ExternalProvider.Lti.BusinessLogic.Models;
using StudioKit.ExternalProvider.Lti.BusinessLogic.Services;
using StudioKit.ExternalProvider.Lti.Models;
using StudioKit.ExternalProvider.Lti.Properties;
using StudioKit.ExternalProvider.Lti.Tests.EqualityComparers;
using StudioKit.ExternalProvider.Lti.Tests.TestData;
using StudioKit.ExternalProvider.Lti.Tests.TestData.DataAccess;
using StudioKit.ExternalProvider.Models.Interfaces;
using StudioKit.ExternalProvider.Tests.TestData.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace StudioKit.ExternalProvider.Lti.Tests.BusinessLogic.Services
{
	[TestClass]
	public class LtiServiceTests : BaseTest
	{
		private SigningCredentials _signingCredentials;
		private JsonWebKeySet _jsonWebKeySet;
		private ILtiKeySetProviderService _keySetProviderService;
		private ILtiConfigurationProvider _configurationProvider;

		[TestInitialize]
		public void Initialize()
		{
			var (signingCredentials, jsonWebKeySet) = TestUtils.GetCredentials();
			_signingCredentials = signingCredentials;
			_jsonWebKeySet = jsonWebKeySet;

			var mockKeySetProviderService = new Mock<ILtiKeySetProviderService>();
			mockKeySetProviderService.Setup(x => x.GetKeySetAsync(It.IsAny<LtiExternalProvider>(), It.IsAny<CancellationToken>())).ReturnsAsync(_jsonWebKeySet);
			_keySetProviderService = mockKeySetProviderService.Object;

			var mockConfigurationProvider = new Mock<ILtiConfigurationProvider>();
			_configurationProvider = mockConfigurationProvider.Object;
		}

		#region GetLaunchReturnUrl

		[TestMethod]
		public void GetLaunchReturnUrl_ShouldThrowIfNullTokenString()
		{
			using (var dbContext = new TestLtiLaunchDbContext(DbConnectionFactory.CreateTransient()))
			{
				var ltiService = GetLtiService(dbContext);
				Assert.ThrowsAsync<ArgumentNullException>(
					Task.Run(() =>
					{
						ltiService.GetLaunchReturnUrl(null);
					}), $"Value cannot be null.{Environment.NewLine}Parameter name: tokenString");

				Assert.ThrowsAsync<ArgumentNullException>(
					Task.Run(() =>
					{
						ltiService.GetLaunchReturnUrl("");
					}), $"Value cannot be null.{Environment.NewLine}Parameter name: tokenString");
			}
		}

		[TestMethod]
		public void GetLaunchReturnUrl_ShouldReturnUrlIfPresent()
		{
			using (var dbContext = new TestLtiLaunchDbContext(DbConnectionFactory.CreateTransient()))
			{
				var ltiService = GetLtiService(dbContext);
				var token = TestUtils.GetToken(_signingCredentials);
				var launchReturnUrl = ltiService.GetLaunchReturnUrl(TestUtils.GetTokenString(token));
				Assert.AreEqual("https://lti-ri.imsglobal.org/platforms/19/returns", launchReturnUrl);
			}
		}

		[TestMethod]
		public void GetLaunchReturnUrl_ShouldReturnNullIfNoPresentationClaim()
		{
			using (var dbContext = new TestLtiLaunchDbContext(DbConnectionFactory.CreateTransient()))
			{
				var ltiService = GetLtiService(dbContext);
				var claimsDictionary = TestUtils.GetClaimsDictionary();
				claimsDictionary.Remove(LtiClaim.LaunchPresentation);
				var token = TestUtils.GetToken(_signingCredentials, TestUtils.Issuer, TestUtils.Audience, TestUtils.GetClaims(claimsDictionary));
				var launchReturnUrl = ltiService.GetLaunchReturnUrl(TestUtils.GetTokenString(token));
				Assert.IsNull(launchReturnUrl);
			}
		}

		[TestMethod]
		public void GetLaunchReturnUrl_ShouldReturnNullIfNoReturnUrlInPresentationClaim()
		{
			using (var dbContext = new TestLtiLaunchDbContext(DbConnectionFactory.CreateTransient()))
			{
				var ltiService = GetLtiService(dbContext);
				var claimsDictionary = TestUtils.GetClaimsDictionary();
				claimsDictionary[LtiClaim.LaunchPresentation] = new Dictionary<string, object>
				{
					{"document_target", "iframe"},
					{"height", 320},
					{"width", 240}
				};
				var token = TestUtils.GetToken(_signingCredentials, TestUtils.Issuer, TestUtils.Audience, TestUtils.GetClaims(claimsDictionary));
				var launchReturnUrl = ltiService.GetLaunchReturnUrl(TestUtils.GetTokenString(token));
				Assert.IsNull(launchReturnUrl);
			}
		}

		#endregion GetLaunchReturnUrl

		#region GetLoginInfoAsync

		[TestMethod]
		public void GetLoginInfoAsync_ShouldThrowIfNullOrEmptyTokenString()
		{
			using (var dbContext = new TestLtiLaunchDbContext(DbConnectionFactory.CreateTransient()))
			{
				var ltiService = GetLtiService(dbContext);
				Assert.ThrowsAsync<ArgumentNullException>(
					Task.Run(async () =>
					{
						await ltiService.GetLoginInfoAsync(null);
					}), $"Value cannot be null.{Environment.NewLine}Parameter name: tokenString");

				Assert.ThrowsAsync<ArgumentNullException>(
					Task.Run(async () =>
					{
						await ltiService.GetLoginInfoAsync("");
					}), $"Value cannot be null.{Environment.NewLine}Parameter name: tokenString");
			}
		}

		[TestMethod]
		public async Task GetLoginInfoAsync_ShouldSucceedWithValidToken()
		{
			using (var dbContext = new TestLtiLaunchDbContext(DbConnectionFactory.CreateTransient()))
			{
				await SeedAsync(dbContext);
				var ltiService = GetLtiService(dbContext);
				var token = TestUtils.GetToken(_signingCredentials);
				var tokenString = TestUtils.GetTokenString(token);
				var externalLoginInfo = await ltiService.GetLoginInfoAsync(tokenString);
				Assert.IsNotNull(externalLoginInfo);
				Assert.AreEqual(TestUtils.Email, externalLoginInfo.DefaultUserName);
				Assert.AreEqual(TestUtils.Email, externalLoginInfo.Email);
				Assert.AreEqual(string.Format(Strings.LoginProviderFormat, "1"), externalLoginInfo.Login.LoginProvider);
				Assert.AreEqual(TestUtils.Sub, externalLoginInfo.Login.ProviderKey);
				Assert.AreEqual(31, externalLoginInfo.ExternalIdentity.Claims.Count());
			}
		}

		#endregion GetLoginInfoAsync

		#region GetLaunchResponseAsync

		[TestMethod]
		public void GetLaunchResponseAsync_ShouldThrowIfNullOrEmptyTokenString()
		{
			using (var dbContext = new TestLtiLaunchDbContext(DbConnectionFactory.CreateTransient()))
			{
				var ltiService = GetLtiService(dbContext);
				Assert.ThrowsAsync<ArgumentNullException>(
					Task.Run(async () =>
					{
						await ltiService.GetLaunchResponseAsync(null, null, null);
					}), $"Value cannot be null.{Environment.NewLine}Parameter name: tokenString");

				Assert.ThrowsAsync<ArgumentNullException>(
					Task.Run(async () =>
					{
						await ltiService.GetLaunchResponseAsync("", null, null);
					}), $"Value cannot be null.{Environment.NewLine}Parameter name: tokenString");
			}
		}

		[TestMethod]
		public void GetLaunchResponseAsync_ShouldThrowIfNullOrEmptyUserId()
		{
			using (var dbContext = new TestLtiLaunchDbContext(DbConnectionFactory.CreateTransient()))
			{
				var ltiService = GetLtiService(dbContext);
				var token = TestUtils.GetToken(_signingCredentials);
				var tokenString = TestUtils.GetTokenString(token);

				Assert.ThrowsAsync<ArgumentNullException>(
					Task.Run(async () =>
					{
						await ltiService.GetLaunchResponseAsync(tokenString, null, null);
					}), $"Value cannot be null.{Environment.NewLine}Parameter name: userId");

				Assert.ThrowsAsync<ArgumentNullException>(
					Task.Run(async () =>
					{
						await ltiService.GetLaunchResponseAsync(tokenString, "", null);
					}), $"Value cannot be null.{Environment.NewLine}Parameter name: userId");
			}
		}

		[TestMethod]
		public async Task GetLaunchResponseAsync_ShouldCreateLtiLaunchForInstructor()
		{
			using (var dbContext = new TestLtiLaunchDbContext(DbConnectionFactory.CreateTransient()))
			{
				await SeedAsync(dbContext);
				dbContext.Users.Add(new TestUser { Id = "1", UserName = "1" });
				await dbContext.SaveChangesAsync();

				var ltiService = GetLtiService(dbContext);
				var token = TestUtils.GetToken(_signingCredentials);
				var tokenString = TestUtils.GetTokenString(token);
				var externalLoginInfo = await ltiService.GetLoginInfoAsync(tokenString);
				var launchResponse = await ltiService.GetLaunchResponseAsync(tokenString, "1", externalLoginInfo.ExternalIdentity.Claims.ToList());
				Assert.IsNotNull(launchResponse.LtiLaunchId);
				Assert.IsNull(launchResponse.GroupId);

				var ltiLaunch =
					await dbContext.LtiLaunches.SingleOrDefaultAsync(d => d.Id.Equals(launchResponse.LtiLaunchId.Value));
				Assert.IsNotNull(ltiLaunch);
				var expected = new LtiLaunch
				{
					ExternalProviderId = 1,
					CreatedById = "1",
					ExternalId = TestUtils.ContextId,
					Name = TestUtils.ContextTitle,
					Description = TestUtils.ContextLabel,
					Roles = LtiRole.MembershipInstructor,
					LtiVersion = LtiVersion.Lti1P3,
					ReturnUrl = TestUtils.LaunchPresentationReturnUrl,
					RosterUrl = TestUtils.NamesRoleServiceMembershipsUrl,
					GradesUrl = TestUtils.EndpointLineItems
				};
				Assert.IsTrue(new LtiLaunchEqualityComparer().Equals(expected, ltiLaunch));
			}
		}

		[TestMethod]
		public async Task GetLaunchResponseAsync_ShouldThrowIfLearnerAndContextNotSetup()
		{
			using (var dbContext = new TestLtiLaunchDbContext(DbConnectionFactory.CreateTransient()))
			{
				await SeedAsync(dbContext);
				dbContext.Users.Add(new TestUser { Id = "1", UserName = "1" });
				await dbContext.SaveChangesAsync();

				var ltiService = GetLtiService(dbContext);
				var claimsDictionary = TestUtils.GetClaimsDictionary();
				claimsDictionary[LtiClaim.Roles] = new[] { LtiRole.MembershipLearner };
				var claims = TestUtils.GetClaims(claimsDictionary);
				var token = TestUtils.GetToken(_signingCredentials, TestUtils.Issuer, TestUtils.Audience, claims);
				var tokenString = TestUtils.GetTokenString(token);
				var externalLoginInfo = await ltiService.GetLoginInfoAsync(tokenString);

				Assert.ThrowsAsync<ForbiddenException>(
					Task.Run(async () =>
					{
						await ltiService.GetLaunchResponseAsync(tokenString, "1", externalLoginInfo.ExternalIdentity.Claims.ToList());
					}), Strings.ContentNotAvailable);
			}
		}

		[TestMethod]
		public async Task GetLaunchResponseAsync_ShouldReturnGroupIdIfContextIsSetup()
		{
			using (var dbContext = new TestLtiLaunchDbContext(DbConnectionFactory.CreateTransient()))
			{
				await SeedAsync(dbContext);
				dbContext.Users.Add(new TestUser { Id = "1", UserName = "1" });
				var group = new TestGroup
				{
					DateStored = DateTime.UtcNow,
					DateLastUpdated = DateTime.UtcNow
				};
				dbContext.Groups.Add(group);
				dbContext.ExternalGroups.Add(new TestExternalGroup
				{
					Group = group,
					ExternalProviderId = 1,
					ExternalId = TestUtils.ContextId,
					UserId = "1",
					Name = TestUtils.ContextTitle,
					Description = TestUtils.ContextLabel,
					DateStored = DateTime.UtcNow,
					DateLastUpdated = DateTime.UtcNow
				});
				await dbContext.SaveChangesAsync();

				var ltiService = GetLtiService(dbContext);
				var token = TestUtils.GetToken(_signingCredentials);
				var tokenString = TestUtils.GetTokenString(token);
				var externalLoginInfo = await ltiService.GetLoginInfoAsync(tokenString);
				var launchResponse = await ltiService.GetLaunchResponseAsync(tokenString, "1", externalLoginInfo.ExternalIdentity.Claims.ToList());
				Assert.IsNull(launchResponse.LtiLaunchId);
				Assert.AreEqual(group.Id, launchResponse.GroupId);
			}
		}

		[TestMethod]
		public async Task GetLaunchResponseAsync_ShouldUpdateExternalGroupDetails()
		{
			using (var dbContext = new TestLtiLaunchDbContext(DbConnectionFactory.CreateTransient()))
			{
				await SeedAsync(dbContext);
				dbContext.Users.Add(new TestUser { Id = "1", UserName = "1" });
				var group = new TestGroup
				{
					DateStored = DateTime.UtcNow,
					DateLastUpdated = DateTime.UtcNow
				};
				dbContext.Groups.Add(group);
				var externalGroup = new TestExternalGroup
				{
					Group = group,
					ExternalProviderId = 1,
					ExternalId = TestUtils.ContextId,
					UserId = "1",
					Name = "Old Name",
					Description = "Old Description",
					DateStored = DateTime.UtcNow,
					DateLastUpdated = DateTime.UtcNow
				};
				dbContext.ExternalGroups.Add(externalGroup);
				await dbContext.SaveChangesAsync();

				var ltiService = GetLtiService(dbContext);
				var token = TestUtils.GetToken(_signingCredentials);
				var tokenString = TestUtils.GetTokenString(token);
				var externalLoginInfo = await ltiService.GetLoginInfoAsync(tokenString);
				var launchResponse = await ltiService.GetLaunchResponseAsync(tokenString, "1", externalLoginInfo.ExternalIdentity.Claims.ToList());
				Assert.IsNull(launchResponse.LtiLaunchId);
				Assert.AreEqual(group.Id, launchResponse.GroupId);
				Assert.AreEqual(TestUtils.ContextTitle, externalGroup.Name);
				Assert.AreEqual(TestUtils.ContextLabel, externalGroup.Description);
				Assert.AreEqual(TestUtils.NamesRoleServiceMembershipsUrl, externalGroup.RosterUrl);
			}
		}

		[TestMethod]
		public async Task GetLaunchResponseAsync_ShouldUpdateExternalGroupUserRoles()
		{
			using (var dbContext = new TestLtiLaunchDbContext(DbConnectionFactory.CreateTransient()))
			{
				await SeedAsync(dbContext);
				var groupOwnerRole = await dbContext.Roles.SingleAsync(r => r.Name.Equals(BaseRole.GroupOwner));
				dbContext.Users.Add(new TestUser { Id = "1", UserName = "1" });
				var group = new TestGroup
				{
					DateStored = DateTime.UtcNow,
					DateLastUpdated = DateTime.UtcNow
				};
				dbContext.Groups.Add(group);
				var externalGroup = new TestExternalGroup
				{
					Group = group,
					ExternalProviderId = 1,
					ExternalId = TestUtils.ContextId,
					UserId = "1",
					Name = "Old Name",
					Description = "Old Description",
					DateStored = DateTime.UtcNow,
					DateLastUpdated = DateTime.UtcNow
				};
				dbContext.ExternalGroups.Add(externalGroup);
				await dbContext.SaveChangesAsync();
				dbContext.ExternalGroupUsers.Add(new TestExternalGroupUser
				{
					UserId = "1",
					ExternalGroupId = externalGroup.Id,
					ExternalUserId = TestUtils.Sub,
					Roles = JsonConvert.SerializeObject(new[] { LtiRole.MembershipLearner })
				});
				dbContext.GroupUserRoles.Add(new TestGroupUserRole
				{
					GroupId = group.Id,
					UserId = "1",
					RoleId = groupOwnerRole.Id
				});
				await dbContext.SaveChangesAsync();

				var ltiService = GetLtiService(dbContext);
				var token = TestUtils.GetToken(_signingCredentials);
				var tokenString = TestUtils.GetTokenString(token);
				var externalLoginInfo = await ltiService.GetLoginInfoAsync(tokenString);
				await ltiService.GetLaunchResponseAsync(tokenString, "1", externalLoginInfo.ExternalIdentity.Claims.ToList());

				var externalGroupUser = await dbContext.ExternalGroupUsers.SingleOrDefaultAsync(u =>
					u.UserId.Equals("1") &&
					u.ExternalUserId.Equals(TestUtils.Sub) &&
					u.ExternalGroupId == externalGroup.Id);
				Assert.IsNotNull(externalGroupUser);
				Assert.AreEqual(LtiRole.MembershipInstructor, externalGroupUser?.Roles);
			}
		}

		[TestMethod]
		public async Task GetLaunchResponseAsync_ShouldAddLearnerToGroupIfNew()
		{
			using (var dbContext = new TestLtiLaunchDbContext(DbConnectionFactory.CreateTransient()))
			{
				await SeedAsync(dbContext);
				dbContext.Users.Add(new TestUser { Id = "1", UserName = "1" });
				dbContext.Users.Add(new TestUser { Id = "2", UserName = "2" });
				var group = new TestGroup
				{
					DateStored = DateTime.UtcNow,
					DateLastUpdated = DateTime.UtcNow
				};
				dbContext.Groups.Add(group);
				var externalGroup = new TestExternalGroup
				{
					Group = group,
					ExternalProviderId = 1,
					ExternalId = TestUtils.ContextId,
					UserId = "1",
					Name = TestUtils.ContextTitle,
					Description = TestUtils.ContextLabel,
					DateStored = DateTime.UtcNow,
					DateLastUpdated = DateTime.UtcNow
				};
				dbContext.ExternalGroups.Add(externalGroup);
				await dbContext.SaveChangesAsync();

				var ltiService = GetLtiService(dbContext);
				var claimsDictionary = TestUtils.GetClaimsDictionary();
				var roles = new[] { LtiRole.MembershipLearner };
				claimsDictionary[LtiClaim.Roles] = roles;
				var claims = TestUtils.GetClaims(claimsDictionary);
				var token = TestUtils.GetToken(_signingCredentials, TestUtils.Issuer, TestUtils.Audience, claims);
				var tokenString = TestUtils.GetTokenString(token);
				var externalLoginInfo = await ltiService.GetLoginInfoAsync(tokenString);
				await ltiService.GetLaunchResponseAsync(tokenString, "2", externalLoginInfo.ExternalIdentity.Claims.ToList());

				var externalGroupUser = await dbContext.ExternalGroupUsers.SingleOrDefaultAsync(u =>
					u.UserId.Equals("2") &&
					u.ExternalUserId.Equals(TestUtils.Sub) &&
					u.ExternalGroupId == externalGroup.Id);
				Assert.IsNotNull(externalGroupUser);
				Assert.AreEqual(LtiRole.MembershipLearner, externalGroupUser?.Roles);

				var groupUserRole = await dbContext.GroupUserRoles.SingleOrDefaultAsync(r =>
					r.UserId.Equals("2") && r.GroupId.Equals(group.Id) && r.Role.Name.Equals(BaseRole.GroupLearner));
				Assert.IsNotNull(groupUserRole);

				var groupUserRoleLog = await dbContext.GroupUserRoleLogs.SingleOrDefaultAsync(l =>
					l.UserId.Equals("2") && l.GroupId.Equals(group.Id) && l.Role.Name.Equals(BaseRole.GroupLearner) &&
					l.Type == GroupUserRoleLogType.Added);
				Assert.IsNotNull(groupUserRoleLog);
			}
		}

		#endregion GetLaunchResponseAsync

		#region AssertLaunchClaimsAreValid

		[TestMethod]
		public void AssertLaunchClaimsAreValid_ShouldSucceed()
		{
			using (var dbContext = new TestLtiLaunchDbContext(DbConnectionFactory.CreateTransient()))
			{
				var ltiService = GetLtiService(dbContext);
				var claimsDictionary = TestUtils.GetClaimsDictionary();
				var claims = TestUtils.GetClaims(claimsDictionary).ToList();
				ltiService.AssertLaunchClaimsAreValid(claims);
			}
		}

		[TestMethod]
		public void AssertLaunchClaimsAreValid_ShouldThrowIfInvalidMessageType()
		{
			using (var dbContext = new TestLtiLaunchDbContext(DbConnectionFactory.CreateTransient()))
			{
				var ltiService = GetLtiService(dbContext);
				Assert.ThrowsAsync<ValidationException>(
					Task.Run(() =>
					{
						var claimsDictionary = TestUtils.GetClaimsDictionary();
						claimsDictionary[LtiClaim.MessageType] = "invalid";
						var claims = TestUtils.GetClaims(claimsDictionary).ToList();
						ltiService.AssertLaunchClaimsAreValid(claims);
					}), string.Format(Strings.TokenMissingRequiredClaimAndValue, LtiClaim.MessageType, LtiMessageType.LtiResourceLinkRequest));
			}
		}

		[TestMethod]
		public void AssertLaunchClaimsAreValid_ShouldThrowIfInvalidVersion()
		{
			using (var dbContext = new TestLtiLaunchDbContext(DbConnectionFactory.CreateTransient()))
			{
				var ltiService = GetLtiService(dbContext);
				Assert.ThrowsAsync<ValidationException>(
					Task.Run(() =>
					{
						var claimsDictionary = TestUtils.GetClaimsDictionary();
						claimsDictionary[LtiClaim.Version] = "2.0.0";
						var claims = TestUtils.GetClaims(claimsDictionary).ToList();
						ltiService.AssertLaunchClaimsAreValid(claims);
					}), string.Format(Strings.TokenMissingRequiredClaimAndValue, LtiClaim.Version, LtiVersion.Lti1P3));
			}
		}

		[TestMethod]
		public void AssertLaunchClaimsAreValid_ShouldThrowIfNoResourceLink()
		{
			using (var dbContext = new TestLtiLaunchDbContext(DbConnectionFactory.CreateTransient()))
			{
				var ltiService = GetLtiService(dbContext);
				Assert.ThrowsAsync<ValidationException>(
					Task.Run(() =>
					{
						var claimsDictionary = TestUtils.GetClaimsDictionary();
						claimsDictionary.Remove(LtiClaim.ResourceLink);
						var claims = TestUtils.GetClaims(claimsDictionary).ToList();
						ltiService.AssertLaunchClaimsAreValid(claims);
					}), string.Format(Strings.TokenMissingRequiredClaim, LtiClaim.ResourceLink));
			}
		}

		[TestMethod]
		public void AssertLaunchClaimsAreValid_ShouldThrowIfNoResourceLinkId()
		{
			using (var dbContext = new TestLtiLaunchDbContext(DbConnectionFactory.CreateTransient()))
			{
				var ltiService = GetLtiService(dbContext);
				Assert.ThrowsAsync<ValidationException>(
					Task.Run(() =>
					{
						var claimsDictionary = TestUtils.GetClaimsDictionary();
						claimsDictionary[LtiClaim.ResourceLink] = new Dictionary<string, object> {
							{"title", "Test Link 1"},
							{"description", "First Test Link"}
						};
						var claims = TestUtils.GetClaims(claimsDictionary).ToList();
						ltiService.AssertLaunchClaimsAreValid(claims);
					}), string.Format(Strings.TokenMissingRequiredClaim, $"{LtiClaim.ResourceLink}.id"));
			}
		}

		[TestMethod]
		public void AssertLaunchClaimsAreValid_ShouldThrowIfNoContext()
		{
			using (var dbContext = new TestLtiLaunchDbContext(DbConnectionFactory.CreateTransient()))
			{
				var ltiService = GetLtiService(dbContext);
				Assert.ThrowsAsync<ValidationException>(
					Task.Run(() =>
					{
						var claimsDictionary = TestUtils.GetClaimsDictionary();
						claimsDictionary.Remove(LtiClaim.Context);
						var claims = TestUtils.GetClaims(claimsDictionary).ToList();
						ltiService.AssertLaunchClaimsAreValid(claims);
					}), string.Format(Strings.TokenMissingRequiredClaim, LtiClaim.Context));
			}
		}

		[TestMethod]
		public void AssertLaunchClaimsAreValid_ShouldThrowIfNoContextId()
		{
			using (var dbContext = new TestLtiLaunchDbContext(DbConnectionFactory.CreateTransient()))
			{
				var ltiService = GetLtiService(dbContext);
				Assert.ThrowsAsync<ValidationException>(
					Task.Run(() =>
					{
						var claimsDictionary = TestUtils.GetClaimsDictionary();
						claimsDictionary[LtiClaim.Context] = new Dictionary<string, object> {
							{"label", TestUtils.ContextLabel},
							{"title", TestUtils.ContextTitle},
							{"type", new []{"Course Section"}}
						};
						var claims = TestUtils.GetClaims(claimsDictionary).ToList();
						ltiService.AssertLaunchClaimsAreValid(claims);
					}), string.Format(Strings.TokenMissingRequiredClaim, $"{LtiClaim.Context}.id"));
			}
		}

		[TestMethod]
		public void AssertLaunchClaimsAreValid_ShouldThrowIfNoEmail()
		{
			using (var dbContext = new TestLtiLaunchDbContext(DbConnectionFactory.CreateTransient()))
			{
				var ltiService = GetLtiService(dbContext);
				Assert.ThrowsAsync<ValidationException>(
					Task.Run(() =>
					{
						var claimsDictionary = TestUtils.GetClaimsDictionary();
						claimsDictionary.Remove(LtiClaim.Email);
						var claims = TestUtils.GetClaims(claimsDictionary).ToList();
						ltiService.AssertLaunchClaimsAreValid(claims);
					}), string.Format(Strings.TokenMissingRequiredClaim, LtiClaim.Email));
			}
		}

		#endregion AssertLaunchClaimsAreValid

		#region Helpers

		private LtiService<TestGroupUserRole, TestGroupUserRoleLog, TestExternalGroup, TestExternalGroupUser> GetLtiService(TestLtiLaunchDbContext dbContext)
		{
			return new LtiService<TestGroupUserRole, TestGroupUserRoleLog, TestExternalGroup, TestExternalGroupUser>(
				dbContext, new JwtTokenService<TestGroupUserRole, TestGroupUserRoleLog, TestExternalGroup, TestExternalGroupUser>(dbContext, _keySetProviderService, _configurationProvider));
		}

		private static async Task SeedAsync(TestLtiLaunchDbContext dbContext, CancellationToken cancellationToken = default(CancellationToken))
		{
			((DbSet<Role>)dbContext.Roles).AddRange(new List<Role>
			{
				new Role(BaseRole.SuperAdmin),
				new Role(BaseRole.Admin),
				new Role(BaseRole.Creator),
				new Role(BaseRole.GroupOwner),
				new Role(BaseRole.GroupLearner)
			});
			dbContext.LtiExternalProviders.Add(new LtiExternalProvider
			{
				Name = "Lti Unit Test Provider",
				Issuer = TestUtils.Issuer,
				OauthClientId = TestUtils.Audience,
				DeploymentId = TestUtils.DeploymentId.ToString(),
				OauthTokenUrl = "dummy",
				KeySetUrl = "dummy",
				DateStored = DateTime.UtcNow,
				DateLastUpdated = DateTime.UtcNow
			});
			await dbContext.SaveChangesAsync(cancellationToken);
		}

		#endregion Helpers
	}
}