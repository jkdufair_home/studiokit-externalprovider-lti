﻿using FluentValidation;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using MSTestExtensions;
using StudioKit.Data.Entity.Identity;
using StudioKit.ExternalProvider.BusinessLogic.Models;
using StudioKit.ExternalProvider.Lti.BusinessLogic.Interfaces;
using StudioKit.ExternalProvider.Lti.BusinessLogic.Models;
using StudioKit.ExternalProvider.Lti.BusinessLogic.Services;
using StudioKit.ExternalProvider.Lti.Models;
using StudioKit.ExternalProvider.Lti.Properties;
using StudioKit.ExternalProvider.Tests.TestData.EqualityComparers;
using StudioKit.ExternalProvider.Tests.TestData.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Headers;
using System.Threading;
using System.Threading.Tasks;

namespace StudioKit.ExternalProvider.Lti.Tests.BusinessLogic.Services
{
	[TestClass]
	public class LtiRosterProviderServiceTests : BaseTest
	{
		private ILtiApiService _ltiApiService;
		private MembershipContainer _membershipContainer;

		[TestInitialize]
		public void Initialize()
		{
			_membershipContainer = new MembershipContainer
			{
				Id = "https://lti-ri.imsglobal.org/platforms/19/contexts/17/memberships.json",
				Members = new List<Member>
				{
					new Member
					{
						ContextId = "17",
						ContextLabel = "Test Course",
						ContextTitle = "A test course from the LTI test tool",
						Name = "Cleveland Lukas Blob",
						Picture = "http://example.org/Cleveland.jpg",
						GivenName = "Cleveland",
						FamilyName = "Blob",
						MiddleName = "Lukas",
						Email = "Cleveland.Blob@example.org",
						UserId = "e51bfcf77f6c6b30ce4c",
						Roles = new List<string>
						{
							LtiRole.MembershipInstructor
						}
					},
					new Member
					{
						ContextId = "17",
						ContextLabel = "Test Course",
						ContextTitle = "A test course from the LTI test tool",
						Name = "Taurean Marlon Roob",
						Picture = "http://example.org/Taurean.jpg",
						GivenName = "Taurean",
						FamilyName = "Roob",
						MiddleName = "Marlon",
						Email = "Taurean.Roob@example.org",
						UserId = "50024ef0fe0de638781b",
						Roles = new List<string>
						{
							LtiRole.MembershipLearner
						}
					},
					new Member
					{
						ContextId = "17",
						ContextLabel = "Test Course",
						ContextTitle = "A test course from the LTI test tool",
						Name = "Margarette Hettie Cummings",
						Picture = "http://example.org/Margarette.jpg",
						GivenName = "Margarette",
						FamilyName = "Cummings",
						MiddleName = "Hettie",
						Email = "Margarette.Cummings@example.org",
						UserId = "3f49327c40b006bf001e",
						Roles = new List<string>
						{
							LtiRole.MembershipLearner
						}
					},
					new Member
					{
						ContextId = "17",
						ContextLabel = "Test Course",
						ContextTitle = "A test course from the LTI test tool",
						Name = "Gianni Ken Rutherford",
						Picture = "http://example.org/Gianni.jpg",
						GivenName = "Gianni",
						FamilyName = "Rutherford",
						MiddleName = "Ken",
						Email = "Gianni.Rutherford@example.org",
						UserId = "d1830007bd4f0e2f1947",
						Roles = new List<string>
						{
							LtiRole.MembershipLearner
						}
					},
					new Member
					{
						ContextId = "17",
						ContextLabel = "Test Course",
						ContextTitle = "A test course from the LTI test tool",
						Name = "Alexys Caterina Feest",
						Picture = "http://example.org/Alexys.jpg",
						GivenName = "Alexys",
						FamilyName = "Feest",
						MiddleName = "Caterina",
						Email = "Alexys.Feest@example.org",
						UserId = "57252576ca62a9722c25",
						Roles = new List<string>
						{
							LtiRole.MembershipMentor
						}
					}
				}
			};

			var mockLtiApiService = new Mock<ILtiApiService>();
			mockLtiApiService
				.Setup(s => s.GetResultAsync<MembershipContainer>(
					It.IsAny<LtiExternalProvider>(),
					It.IsAny<string>(),
					It.IsAny<CancellationToken>(),
					It.IsAny<MediaTypeWithQualityHeaderValue>()))
				.ReturnsAsync(_membershipContainer);
			_ltiApiService = mockLtiApiService.Object;
		}

		[TestMethod]
		public void GetRosterAsync_ShouldThrowIfNullArguments()
		{
			var service = GetService();
			Assert.ThrowsAsync<ArgumentNullException>(
				Task.Run(async () =>
				{
					await service.GetRosterAsync(null, null, CancellationToken.None);
				}), $"Value cannot be null.{Environment.NewLine}Parameter name: externalProvider");

			Assert.ThrowsAsync<ArgumentNullException>(
				Task.Run(async () =>
				{
					await service.GetRosterAsync(new LtiExternalProvider(), null, CancellationToken.None);
				}), $"Value cannot be null.{Environment.NewLine}Parameter name: externalGroup");
		}

		[TestMethod]
		public void GetRosterAsync_ShouldThrowIfNotLtiExternalProvider()
		{
			var service = GetService();
			Assert.ThrowsAsync<ArgumentException>(
				Task.Run(async () =>
				{
					await service.GetRosterAsync(new TestExternalProvider(), new TestExternalGroup(), CancellationToken.None);
				}), Strings.LtiExternalProviderRequired);
		}

		[TestMethod]
		public void GetRosterAsync_ShouldThrowIfAnyMembersAreMissingEmail()
		{
			_membershipContainer.Members.First().Email = null;

			var externalGroup = new TestExternalGroup
			{
				Id = 1
			};

			var service = GetService();
			Assert.ThrowsAsync<ValidationException>(
				Task.Run(async () =>
				{
					await service.GetRosterAsync(new LtiExternalProvider(), externalGroup, CancellationToken.None);
				}), Strings.MembersMissingEmail);
		}

		[TestMethod]
		public async Task GetRosterAsync_ShouldReturnInstructorsAndLearners()
		{
			var externalGroup = new TestExternalGroup
			{
				Id = 1
			};
			var expectedResult = new List<ExternalRosterEntry<TestExternalGroup, TestExternalGroupUser>>
			{
				new ExternalRosterEntry<TestExternalGroup, TestExternalGroupUser>
				{
					ExternalGroupUser = new TestExternalGroupUser
					{
						ExternalGroupId = externalGroup.Id,
						ExternalUserId = "e51bfcf77f6c6b30ce4c",
						User = new TestUser
						{
							UserName = "Cleveland.Blob@example.org",
							Email = "Cleveland.Blob@example.org",
							FirstName = "Cleveland",
							LastName = "Blob",
							EmployeeNumber = "e51bfcf77f6c6b30ce4c",
							Uid = "Cleveland.Blob"
						}
					},
					GroupUserRoles = new List<string>
					{
						BaseRole.GroupOwner
					}
				},
				new ExternalRosterEntry<TestExternalGroup, TestExternalGroupUser>
				{
					ExternalGroupUser = new TestExternalGroupUser
					{
						ExternalGroupId = externalGroup.Id,
						ExternalUserId = "50024ef0fe0de638781b",
						User = new TestUser
						{
							UserName = "Taurean.Roob@example.org",
							Email = "Taurean.Roob@example.org",
							FirstName = "Taurean",
							LastName = "Roob",
							EmployeeNumber = "50024ef0fe0de638781b",
							Uid = "Taurean.Roob"
						}
					},
					GroupUserRoles = new List<string>
					{
						BaseRole.GroupLearner
					}
				},
				new ExternalRosterEntry<TestExternalGroup, TestExternalGroupUser>
				{
					ExternalGroupUser = new TestExternalGroupUser
					{
						ExternalGroupId = externalGroup.Id,
						ExternalUserId = "3f49327c40b006bf001e",
						User = new TestUser
						{
							UserName = "Margarette.Cummings@example.org",
							Email = "Margarette.Cummings@example.org",
							FirstName = "Margarette",
							LastName = "Cummings",
							EmployeeNumber = "3f49327c40b006bf001e",
							Uid = "Margarette.Cummings"
						}
					},
					GroupUserRoles = new List<string>
					{
						BaseRole.GroupLearner
					}
				},
				new ExternalRosterEntry<TestExternalGroup, TestExternalGroupUser>
				{
					ExternalGroupUser = new TestExternalGroupUser
					{
						ExternalGroupId = externalGroup.Id,
						ExternalUserId = "d1830007bd4f0e2f1947",
						User = new TestUser
						{
							UserName = "Gianni.Rutherford@example.org",
							Email = "Gianni.Rutherford@example.org",
							FirstName = "Gianni",
							LastName = "Rutherford",
							EmployeeNumber = "d1830007bd4f0e2f1947",
							Uid = "Gianni.Rutherford"
						}
					},
					GroupUserRoles = new List<string>
					{
						BaseRole.GroupLearner
					}
				}
			};

			var service = GetService();
			var rosterEntries = await service.GetRosterAsync(new LtiExternalProvider(), externalGroup, CancellationToken.None);
			Assert.IsTrue(rosterEntries.SequenceEqual(expectedResult, new ExternalRosterEntryEqualityComparer()));
		}

		#region Helpers

		private LtiRosterProviderService<TestUser, TestExternalGroup, TestExternalGroupUser> GetService()
		{
			return new LtiRosterProviderService<TestUser, TestExternalGroup, TestExternalGroupUser>(_ltiApiService);
		}

		#endregion Helpers
	}
}