﻿using Effort;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using MSTestExtensions;
using Newtonsoft.Json;
using StudioKit.ErrorHandling.Exceptions;
using StudioKit.ExternalProvider.Lti.BusinessLogic.Interfaces;
using StudioKit.ExternalProvider.Lti.BusinessLogic.Models;
using StudioKit.ExternalProvider.Lti.BusinessLogic.Services;
using StudioKit.ExternalProvider.Lti.Models;
using StudioKit.ExternalProvider.Lti.Properties;
using StudioKit.ExternalProvider.Lti.Tests.TestData;
using StudioKit.ExternalProvider.Lti.Tests.TestData.DataAccess;
using StudioKit.ExternalProvider.Models;
using StudioKit.ExternalProvider.Tests.TestData.Models;
using StudioKit.Security;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Claims;
using System.Security.Principal;
using System.Threading;
using System.Threading.Tasks;

namespace StudioKit.ExternalProvider.Lti.Tests.BusinessLogic.Services
{
	[TestClass]
	public class LtiGradePushServiceTests : BaseTest
	{
		private ILtiApiService _ltiApiService;
		private LtiExternalProvider _ltiExternalProvider;
		private TestGroupAssignment _groupAssignment;
		private int _lineItemCount;
		private int _resultCount;
		private List<TestUser> _students;
		private TestExternalGroup _externalGroup1;
		private List<TestExternalGroupUser> _externalGroup1Students;
		private List<TestGroupAssignmentScore> _scores;
		private TestExternalGroup _externalGroup2;
		private List<TestExternalGroupUser> _externalGroup2Students;

		[TestInitialize]
		public void Initialize()
		{
			var mockLtiApiService = new Mock<ILtiApiService>();

			mockLtiApiService.Setup(x => x.PostAsync<LineItem>(
					It.IsAny<LtiExternalProvider>(),
					It.IsAny<string>(),
					It.IsAny<Func<Uri, HttpContent>>(),
					It.IsAny<CancellationToken>(),
					It.IsAny<MediaTypeWithQualityHeaderValue>()))
				.Returns(async (LtiExternalProvider ltiExternalProvider, string url, Func<Uri, HttpContent> getHttpContent,
						CancellationToken cancellationToken, MediaTypeWithQualityHeaderValue acceptHeaderValue) =>
				{
					var stringContent = (StringContent)getHttpContent(new Uri(url));
					var stringContentAsString = await stringContent.ReadAsStringAsync();
					var lineItem = JsonConvert.DeserializeObject<LineItem>(stringContentAsString);
					_lineItemCount++;
					lineItem.Id = $"{url}/{_lineItemCount}";
					return lineItem;
				});

			mockLtiApiService.Setup(x => x.PostAsync<Result>(
					It.IsAny<LtiExternalProvider>(),
					It.IsAny<string>(),
					It.IsAny<Func<Uri, HttpContent>>(),
					It.IsAny<CancellationToken>(),
					It.IsAny<MediaTypeWithQualityHeaderValue>()))
				.Returns(async (LtiExternalProvider ltiExternalProvider, string url, Func<Uri, HttpContent> getHttpContent,
					CancellationToken cancellationToken, MediaTypeWithQualityHeaderValue acceptHeaderValue) =>
				{
					var stringContent = (StringContent)getHttpContent(new Uri(url));
					var stringContentAsString = await stringContent.ReadAsStringAsync();
					var score = JsonConvert.DeserializeObject<Score>(stringContentAsString);
					var lineItemUrl = url.Replace("/scores", "");
					_resultCount++;
					var result = new Result
					{
						Id = $"{lineItemUrl}/results/{_resultCount}",
						ScoreOf = lineItemUrl,
						UserId = score.UserId,
						ResultScore = score.ScoreGiven,
						ResultMaximum = score.ScoreMaximum,
						Comment = score.Comment
					};
					return result;
				});

			_ltiApiService = mockLtiApiService.Object;

			_ltiExternalProvider = new LtiExternalProvider
			{
				Name = "Lti Unit Test Provider",
				Issuer = TestUtils.Issuer,
				OauthClientId = TestUtils.Audience,
				DeploymentId = TestUtils.DeploymentId.ToString(),
				OauthTokenUrl = "dummy",
				KeySetUrl = "dummy",
				GradePushEnabled = true,
				DateStored = DateTime.UtcNow,
				DateLastUpdated = DateTime.UtcNow
			};

			_externalGroup1 = new TestExternalGroup
			{
				Name = "Test Course",
				ExternalProvider = _ltiExternalProvider,
				ExternalId = "1",
				GradesUrl = "https://lms.edu/courses/1/lineitems",
				DateStored = DateTime.UtcNow,
				DateLastUpdated = DateTime.UtcNow,
				UserId = "1",
				ExternalGroupUsers = new List<TestExternalGroupUser>
				{
					new TestExternalGroupUser
					{
						UserId = "1",
						ExternalUserId = "a",
						DateStored = DateTime.UtcNow,
						DateLastUpdated = DateTime.UtcNow
					}
				}
			};

			_externalGroup2 = new TestExternalGroup
			{
				Name = "Test Course 2",
				ExternalProvider = _ltiExternalProvider,
				ExternalId = "2",
				GradesUrl = "https://lms.edu/courses/2/lineitems",
				DateStored = DateTime.UtcNow,
				DateLastUpdated = DateTime.UtcNow,
				UserId = "1",
				ExternalGroupUsers = new List<TestExternalGroupUser>
				{
					new TestExternalGroupUser
					{
						UserId = "1",
						ExternalUserId = "a",
						DateStored = DateTime.UtcNow,
						DateLastUpdated = DateTime.UtcNow
					}
				}
			};

			_groupAssignment = new TestGroupAssignment
			{
				DateStored = DateTime.UtcNow,
				DateLastUpdated = DateTime.UtcNow,
				Assignment = new TestAssignment
				{
					Name = "Test Assignment",
					Points = 100m,
					DateStored = DateTime.UtcNow,
					DateLastUpdated = DateTime.UtcNow
				},
				Group = new TestGroup
				{
					DateStored = DateTime.UtcNow,
					DateLastUpdated = DateTime.UtcNow,
					ExternalGroups = new List<TestExternalGroup>
					{
						_externalGroup1
					}
				}
			};

			_students = new List<TestUser>
			{
				new TestUser { Id = "2", UserName = "2" },
				new TestUser { Id = "3", UserName = "3" },
				new TestUser { Id = "4", UserName = "4" },
			};

			_externalGroup1Students = new List<TestExternalGroupUser>
			{
				new TestExternalGroupUser
				{
					ExternalGroup = _externalGroup1,
					UserId = "2",
					ExternalUserId = "b",
					DateStored = DateTime.UtcNow,
					DateLastUpdated = DateTime.UtcNow,
				},
				new TestExternalGroupUser
				{
					ExternalGroup = _externalGroup1,
					UserId = "3",
					ExternalUserId = "c",
					DateStored = DateTime.UtcNow,
					DateLastUpdated = DateTime.UtcNow
				},
				new TestExternalGroupUser
				{
					ExternalGroup = _externalGroup1,
					UserId = "4",
					ExternalUserId = "d",
					DateStored = DateTime.UtcNow,
					DateLastUpdated = DateTime.UtcNow
				}
			};

			_externalGroup2Students = new List<TestExternalGroupUser>
			{
				new TestExternalGroupUser
				{
					ExternalGroup = _externalGroup2,
					UserId = "2",
					ExternalUserId = "x",
					DateStored = DateTime.UtcNow,
					DateLastUpdated = DateTime.UtcNow,
				},
				new TestExternalGroupUser
				{
					ExternalGroup = _externalGroup2,
					UserId = "3",
					ExternalUserId = "y",
					DateStored = DateTime.UtcNow,
					DateLastUpdated = DateTime.UtcNow
				},
				new TestExternalGroupUser
				{
					ExternalGroup = _externalGroup2,
					UserId = "4",
					ExternalUserId = "z",
					DateStored = DateTime.UtcNow,
					DateLastUpdated = DateTime.UtcNow
				}
			};

			_scores = new List<TestGroupAssignmentScore>
			{
				new TestGroupAssignmentScore
				{
					GroupAssignment = _groupAssignment,
					CreatedById = "2",
					TotalPoints = 90m,
					DateStored = DateTime.UtcNow,
					DateLastUpdated = DateTime.UtcNow
				},
				new TestGroupAssignmentScore
				{
					GroupAssignment = _groupAssignment,
					CreatedById = "3",
					TotalPoints = 80m,
					DateStored = DateTime.UtcNow,
					DateLastUpdated = DateTime.UtcNow
				},
				new TestGroupAssignmentScore
				{
					GroupAssignment = _groupAssignment,
					CreatedById = "4",
					TotalPoints = 70m,
					DateStored = DateTime.UtcNow,
					DateLastUpdated = DateTime.UtcNow
				}
			};
		}

		[TestMethod]
		public void PushGroupAssignmentGradesAsync_ShouldThrowIfNullPrincipal()
		{
			using (var dbContext = new TestLtiGradePushDbContext(DbConnectionFactory.CreateTransient()))
			{
				var service = GetService(dbContext);
				Assert.ThrowsAsync<ArgumentNullException>(
					Task.Run(async () =>
					{
						await service.PushGroupAssignmentGradesAsync(1, null, CancellationToken.None);
					}), $"Value cannot be null.{Environment.NewLine}Parameter name: principal");
			}
		}

		[TestMethod]
		public void PushGroupAssignmentGradesAsync_ShouldThrowIfPrincipalNotSystem()
		{
			using (var dbContext = new TestLtiGradePushDbContext(DbConnectionFactory.CreateTransient()))
			{
				var service = GetService(dbContext);
				Assert.ThrowsAsync<ForbiddenException>(
					Task.Run(async () =>
					{
						await service.PushGroupAssignmentGradesAsync(1, new GenericPrincipal(new ClaimsIdentity(), null), CancellationToken.None);
					}), string.Format(Security.Properties.Strings.RequiresSystemPrincipal, "PushGroupAssignmentGradesAsync"));
			}
		}

		[TestMethod]
		public void PushGroupAssignmentGradesAsync_ShouldThrowIfGroupAssignmentIsNotFound()
		{
			using (var dbContext = new TestLtiGradePushDbContext(DbConnectionFactory.CreateTransient()))
			{
				var service = GetService(dbContext);
				Assert.ThrowsAsync<EntityNotFoundException>(
					Task.Run(async () =>
					{
						await service.PushGroupAssignmentGradesAsync(1, new SystemPrincipal(), CancellationToken.None);
					}), string.Format(Strings.EntityNotFoundById, nameof(TestGroupAssignment), 1));
			}
		}

		[TestMethod]
		public async Task PushGroupAssignmentGradesAsync_ShouldThrowIfGroupAssignmentIsDeleted()
		{
			using (var dbContext = new TestLtiGradePushDbContext(DbConnectionFactory.CreateTransient()))
			{
				_groupAssignment.IsDeleted = true;
				await SeedAsync(dbContext);

				var service = GetService(dbContext);
				Assert.ThrowsAsync<EntityNotFoundException>(
					Task.Run(async () =>
					{
						await service.PushGroupAssignmentGradesAsync(_groupAssignment.Id, new SystemPrincipal(), CancellationToken.None);
					}), string.Format(Strings.EntityNotFoundById, nameof(TestGroupAssignment), 1));
			}
		}

		[TestMethod]
		public async Task PushGroupAssignmentGradesAsync_ShouldThrowIfAssignmentIsDeleted()
		{
			using (var dbContext = new TestLtiGradePushDbContext(DbConnectionFactory.CreateTransient()))
			{
				_groupAssignment.Assignment.IsDeleted = true;
				await SeedAsync(dbContext);

				var service = GetService(dbContext);
				Assert.ThrowsAsync<EntityNotFoundException>(
					Task.Run(async () =>
					{
						await service.PushGroupAssignmentGradesAsync(_groupAssignment.Id, new SystemPrincipal(), CancellationToken.None);
					}), string.Format(Strings.EntityNotFoundById, nameof(TestAssignment), 1));
			}
		}

		[TestMethod]
		public async Task PushGroupAssignmentGradesAsync_ShouldThrowIfAssignmentHasNoPoints()
		{
			using (var dbContext = new TestLtiGradePushDbContext(DbConnectionFactory.CreateTransient()))
			{
				_groupAssignment.Assignment.Points = null;
				await SeedAsync(dbContext);

				var service = GetService(dbContext);
				Assert.ThrowsAsync<Exception>(
					Task.Run(async () =>
					{
						await service.PushGroupAssignmentGradesAsync(_groupAssignment.Id, new SystemPrincipal(), CancellationToken.None);
					}), Strings.GradePushAssignmentPointsRequired);
			}
		}

		[TestMethod]
		public async Task PushGroupAssignmentGradesAsync_ShouldThrowIfAlreadyPushed()
		{
			using (var dbContext = new TestLtiGradePushDbContext(DbConnectionFactory.CreateTransient()))
			{
				_groupAssignment.Logs = new List<TestGroupAssignmentJobLog>
				{
					new TestGroupAssignmentJobLog
					{
						Type = GradePushGroupAssignmentJobLogType.Pushing,
						DateStored = DateTime.UtcNow.AddHours(-2),
						DateLastUpdated = DateTime.UtcNow.AddHours(-2)
					},
					new TestGroupAssignmentJobLog
					{
						Type = GradePushGroupAssignmentJobLogType.Pushed,
						DateStored = DateTime.UtcNow.AddHours(-1),
						DateLastUpdated = DateTime.UtcNow.AddHours(-1)
					}
				};
				await SeedAsync(dbContext);
				var service = GetService(dbContext);
				Assert.ThrowsAsync<ForbiddenException>(
					Task.Run(async () =>
					{
						await service.PushGroupAssignmentGradesAsync(_groupAssignment.Id, new SystemPrincipal(), CancellationToken.None);
					}), Strings.GradePushAlreadyPushed);
			}
		}

		[TestMethod]
		public async Task PushGroupAssignmentGradesAsync_ShouldThrowIfNoExternalGroups()
		{
			using (var dbContext = new TestLtiGradePushDbContext(DbConnectionFactory.CreateTransient()))
			{
				_groupAssignment.Group.ExternalGroups = null;
				await SeedAsync(dbContext);

				var service = GetService(dbContext);
				Assert.ThrowsAsync<Exception>(
					Task.Run(async () =>
					{
						await service.PushGroupAssignmentGradesAsync(_groupAssignment.Id, new SystemPrincipal(), CancellationToken.None);
					}), Strings.GradePushExternalGroupsRequired);
			}
		}

		[TestMethod]
		public async Task PushGroupAssignmentGradesAsync_ShouldThrowIfNoExternalGroupsWithDisabledExternalProvider()
		{
			using (var dbContext = new TestLtiGradePushDbContext(DbConnectionFactory.CreateTransient()))
			{
				_ltiExternalProvider.GradePushEnabled = false;
				await SeedAsync(dbContext);

				var service = GetService(dbContext);
				Assert.ThrowsAsync<Exception>(
					Task.Run(async () =>
					{
						await service.PushGroupAssignmentGradesAsync(_groupAssignment.Id, new SystemPrincipal(), CancellationToken.None);
					}), Strings.GradePushExternalGroupsRequired);
			}
		}

		[TestMethod]
		public async Task PushGroupAssignmentGradesAsync_ShouldThrowIfNoExternalGroupsWithNoGradesUrl()
		{
			using (var dbContext = new TestLtiGradePushDbContext(DbConnectionFactory.CreateTransient()))
			{
				_groupAssignment.Group.ExternalGroups.First().GradesUrl = null;
				await SeedAsync(dbContext);

				var service = GetService(dbContext);
				Assert.ThrowsAsync<Exception>(
					Task.Run(async () =>
					{
						await service.PushGroupAssignmentGradesAsync(_groupAssignment.Id, new SystemPrincipal(), CancellationToken.None);
					}), Strings.GradePushExternalGroupsRequired);
			}
		}

		[TestMethod]
		public async Task PushGroupAssignmentGradesAsync_ShouldThrowIfNoScores()
		{
			using (var dbContext = new TestLtiGradePushDbContext(DbConnectionFactory.CreateTransient()))
			{
				await SeedAsync(dbContext);

				var service = GetService(dbContext);
				Assert.ThrowsAsync<Exception>(
					Task.Run(async () =>
					{
						await service.PushGroupAssignmentGradesAsync(_groupAssignment.Id, new SystemPrincipal(), CancellationToken.None);
					}), Strings.GradePushScoresRequired);
			}
		}

		[TestMethod]
		public async Task PushGroupAssignmentGradesAsync_ShouldPushGradesToASingleExternalGroup()
		{
			using (var dbContext = new TestLtiGradePushDbContext(DbConnectionFactory.CreateTransient()))
			{
				((DbSet<TestUser>)dbContext.Users).AddRange(_students);
				dbContext.ExternalGroupUsers.AddRange(_externalGroup1Students);
				dbContext.GroupAssignmentScores.AddRange(_scores);
				await SeedAsync(dbContext);

				var service = GetService(dbContext);
				await service.PushGroupAssignmentGradesAsync(_groupAssignment.Id, new SystemPrincipal(), CancellationToken.None);

				await AssertAsync(dbContext, 1, 1, 3);
			}
		}

		[TestMethod]
		public async Task PushGroupAssignmentGradesAsync_ShouldNotPushGradesIfScoreHasNoPoints()
		{
			using (var dbContext = new TestLtiGradePushDbContext(DbConnectionFactory.CreateTransient()))
			{
				((DbSet<TestUser>)dbContext.Users).AddRange(_students);
				dbContext.ExternalGroupUsers.AddRange(_externalGroup1Students);
				dbContext.GroupAssignmentScores.AddRange(_scores);
				_scores.First().TotalPoints = null;
				await SeedAsync(dbContext);

				var service = GetService(dbContext);
				await service.PushGroupAssignmentGradesAsync(_groupAssignment.Id, new SystemPrincipal(), CancellationToken.None);

				await AssertAsync(dbContext, 1, 1, 2);
			}
		}

		[TestMethod]
		public async Task PushGroupAssignmentGradesAsync_ShouldPushGradesToMultipleExternalGroups()
		{
			using (var dbContext = new TestLtiGradePushDbContext(DbConnectionFactory.CreateTransient()))
			{
				((DbSet<TestUser>)dbContext.Users).AddRange(_students);
				dbContext.ExternalGroupUsers.AddRange(_externalGroup1Students);
				dbContext.ExternalGroups.Add(_externalGroup2);
				dbContext.ExternalGroupUsers.AddRange(_externalGroup2Students);
				dbContext.GroupAssignmentScores.AddRange(_scores);
				await SeedAsync(dbContext);

				var service = GetService(dbContext);
				await service.PushGroupAssignmentGradesAsync(_groupAssignment.Id, new SystemPrincipal(), CancellationToken.None);

				await AssertAsync(dbContext, 2, 2, 6);
			}
		}

		[TestMethod]
		public async Task PushGroupAssignmentGradesAsync_ShouldPushGradesIfSomeExternalGroupAssignementsExist()
		{
			using (var dbContext = new TestLtiGradePushDbContext(DbConnectionFactory.CreateTransient()))
			{
				((DbSet<TestUser>)dbContext.Users).AddRange(_students);
				dbContext.ExternalGroupUsers.AddRange(_externalGroup1Students);
				dbContext.ExternalGroups.Add(_externalGroup2);
				dbContext.ExternalGroupUsers.AddRange(_externalGroup2Students);
				dbContext.GroupAssignmentScores.AddRange(_scores);
				dbContext.ExternalGroupAssignments.Add(new TestExternalGroupAssignment
				{
					GroupAssignment = _groupAssignment,
					ExternalGroup = _externalGroup1,
					ExternalId = $"{_externalGroup1.GradesUrl}/999",
					DateStored = DateTime.UtcNow,
					DateLastUpdated = DateTime.UtcNow
				});
				await SeedAsync(dbContext);

				var service = GetService(dbContext);
				await service.PushGroupAssignmentGradesAsync(_groupAssignment.Id, new SystemPrincipal(), CancellationToken.None);

				await AssertAsync(dbContext, 2, 1, 6);
			}
		}

		#region Helpers

		private LtiGradePushService<TestGroup, TestGroupUserRole, TestExternalGroup, TestExternalGroupUser, TestExternalGroupAssignment, TestGroupAssignment, TestAssignment, TestGroupAssignmentScore, TestGroupAssignmentJobLog> GetService(TestLtiGradePushDbContext dbContext)
		{
			return new LtiGradePushService<TestGroup, TestGroupUserRole, TestExternalGroup, TestExternalGroupUser, TestExternalGroupAssignment, TestGroupAssignment, TestAssignment, TestGroupAssignmentScore, TestGroupAssignmentJobLog>(dbContext, _ltiApiService);
		}

		private async Task SeedAsync(TestLtiGradePushDbContext dbContext, CancellationToken cancellationToken = default(CancellationToken))
		{
			dbContext.Users.Add(new TestUser { Id = "1", UserName = "1" });
			dbContext.LtiExternalProviders.Add(_ltiExternalProvider);
			dbContext.GroupAssignments.Add(_groupAssignment);
			await dbContext.SaveChangesAsync(cancellationToken);
		}

		private async Task AssertAsync(TestLtiGradePushDbContext dbContext, int externalGroupAssignmentsCount, int lineItemsCreated, int resultsCreated, CancellationToken cancellationToken = default(CancellationToken))
		{
			// assert - externalGroupAssignment created for externalGroups without one
			var externalGroupAssignments = await dbContext.ExternalGroupAssignments
				.Where(ega => ega.GroupAssignmentId.Equals(_groupAssignment.Id)).ToListAsync(cancellationToken);
			Assert.AreEqual(externalGroupAssignmentsCount, externalGroupAssignments.Count, "Number of ExternalGroupAssignments");
			Assert.AreEqual(lineItemsCreated, _lineItemCount, "Number of LineItems created");

			// assert - POST called correct number of times (externalGroupAssignments * externalGroupUser w/ score)
			Assert.AreEqual(resultsCreated, _resultCount, "Number of Results created");
		}

		#endregion Helpers
	}
}