﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using MSTestExtensions;
using Newtonsoft.Json.Linq;
using StudioKit.ErrorHandling.Interfaces;
using StudioKit.ExternalProvider.Lti.BusinessLogic.Interfaces;
using StudioKit.ExternalProvider.Lti.BusinessLogic.Services;
using StudioKit.ExternalProvider.Lti.Exceptions;
using StudioKit.ExternalProvider.Lti.Models;
using StudioKit.ExternalProvider.Lti.Properties;
using StudioKit.ExternalProvider.Lti.Tests.TestData;
using StudioKit.TransientFaultHandling.Http;
using StudioKit.TransientFaultHandling.Http.Interfaces;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Claims;
using System.Threading;
using System.Threading.Tasks;

namespace StudioKit.ExternalProvider.Lti.Tests.BusinessLogic.Services
{
	[TestClass]
	[DoNotParallelize]
	public class LtiApiServiceTests : BaseTest
	{
		private readonly string TestJwtString =
			"eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJpYXQiOjE1MzQ3ODk5ODUsImp0aSI6Ijk0NDBjODgwLWEyZmQtNDk3OC05ZTFiLTE4NmUzZTA4MjM0MSIsImV4cCI6MTYzNTg5MDE2NCwiaXNzIjoiMTIzNDUiLCJhdWQiOiJodHRwczovL2x0aS1yaS5pbXNnbG9iYWwub3JnL3BsYXRmb3Jtcy8xOS9hY2Nlc3NfdG9rZW5zIiwic3ViIjoiMTIzNDUifQ.jzQlRgPyOEngf86AdYfBIkDd67JHa5re0A1dVk9DIV6MaFdc-FSUJezpzw8mU9XKhe87383rW4Cbmk58dG7e9-2ie_M_uUMlow6B0qnDZu00o7JX6K9bzVE46g1A3pFel8UD05zYbxTF9AYEvTiJhBncBFKMKRucwLfwIAp4EKJGThpzz45XgnYhtzBnmvosubm8kmwh7aaMAxuZ_vuqqsfsvlASVxbYpqR1cpHBO1mYxf1k-dRdJjTBCpm7uC5YM2Rd108rxTyAoWE3LMLDLb1GR5aNnvd40uf1OI48hX21CEG7gzPP_zV2KpbtyRyAFzmNQJ35jDS5prSTUcS-cA";

		private readonly string TestToken =
			"{\"access_token\":\"token\",\"token_type\":\"Bearer\",\"expires_in\":3600,\"scope\":\"https://purl.imsglobal.org/spec/lti-nrps/scope/contextmembership.readonly https://purl.imsglobal.org/spec/lti-ags/scope/lineitem https://purl.imsglobal.org/spec/lti-ags/scope/result.readonly https://purl.imsglobal.org/spec/lti-ags/scope/score\"}";

		//JSON response that can't be de-serialized to any of our models
		private readonly string invalidJsonResponse = "<{\"invalid\":\"object\"}";

		private readonly LtiExternalProvider _ltiExternalProvider = new LtiExternalProvider
		{
			Name = "Lti Unit Test Provider",
			Issuer = TestUtils.Issuer,
			OauthClientId = TestUtils.Audience,
			DeploymentId = TestUtils.DeploymentId.ToString(),
			OauthTokenUrl = "https://lti-ri.imsglobal.org/platforms/19/access_tokens",
			KeySetUrl = "dummy",
			DateStored = DateTime.UtcNow,
			DateLastUpdated = DateTime.UtcNow
		};

		private IErrorHandler _errorHandler;

		private IJwtTokenService _jwtTokenService;

		[TestInitialize]
		public void Initialize()
		{
			var handler = new JwtSecurityTokenHandler();
			var token = handler.CreateJwtSecurityToken(TestJwtString);

			_errorHandler = new Mock<IErrorHandler>().Object;
			var tokenServiceMock = new Mock<IJwtTokenService>();
			tokenServiceMock.Setup(x => x.CreateJwtAsync(It.IsAny<LtiExternalProvider>(), It.IsAny<List<Claim>>(), It.IsAny<CancellationToken>()))
				.ReturnsAsync(token);
			_jwtTokenService = tokenServiceMock.Object;
		}

		#region GetResultAsync

		[TestMethod]
		public void GetResultsAsync_ShouldThrowIfExternalProviderNull()
		{
			var apiService = GetService(() => new RetryingHttpClient());

			Assert.ThrowsAsync<ArgumentNullException>(
				Task.Run(async () =>
				{
					await apiService.GetResultAsync<JObject>(null, "http://www.example.com", CancellationToken.None);
				}), $"Value cannot be null.{Environment.NewLine}Parameter name: ltiExternalProvider");
		}

		[TestMethod]
		public void GetResultsAsync_ShouldThrowIfUrlNull()
		{
			var apiService = GetService(() => new RetryingHttpClient());

			Assert.ThrowsAsync<ArgumentNullException>(
				Task.Run(async () =>
				{
					await apiService.GetResultAsync<JObject>(_ltiExternalProvider, null, CancellationToken.None);
				}), $"Value cannot be null.{Environment.NewLine}Parameter name: url");
		}

		[TestMethod]
		public async Task GetResultsAsync_ShouldSucceedIfGetsToken()
		{
			var httpClientMock = new Mock<IHttpClient>();
			httpClientMock.Setup(x => x.PostAsync(
					It.Is<Uri>(u => u.AbsoluteUri.Equals(_ltiExternalProvider.OauthTokenUrl)),
					It.IsNotNull<Func<Uri, HttpContent>>(),
					It.IsAny<CancellationToken>(),
					It.IsAny<AuthenticationHeaderValue>(),
					It.IsAny<MediaTypeWithQualityHeaderValue>()))
				.ReturnsAsync(TestToken);
			httpClientMock.Setup(x => x.GetStringAsync(
					It.Is<Uri>(u => u.AbsoluteUri.Equals("http://www.example.com/")),
					It.Is<CancellationToken>(t => t.Equals(CancellationToken.None)),
					It.Is<AuthenticationHeaderValue>(ahv => ahv.Scheme.Equals("Bearer") && ahv.Parameter.Equals("token")),
					It.Is<MediaTypeWithQualityHeaderValue>(m => m.MediaType.Equals("application/json"))))
				.ReturnsAsync("{\"test\":\"json\"}");

			var apiService = GetService(() => httpClientMock.Object);

			var result = await apiService.GetResultAsync<JObject>(_ltiExternalProvider, "http://www.example.com", CancellationToken.None,
				new MediaTypeWithQualityHeaderValue("application/json"));

			Assert.AreEqual("json", result.Value<string>("test"));
		}

		[TestMethod]
		public void GetResultsAsync_ShouldThrowOnNetworkException()
		{
			var httpClientMock = new Mock<IHttpClient>();
			httpClientMock.Setup(x => x.PostAsync(
					It.Is<Uri>(u => u.AbsoluteUri.Equals(_ltiExternalProvider.OauthTokenUrl)),
					It.IsNotNull<Func<Uri, HttpContent>>(),
					It.IsAny<CancellationToken>(),
					It.IsAny<AuthenticationHeaderValue>(),
					It.IsAny<MediaTypeWithQualityHeaderValue>()))
				.ReturnsAsync(TestToken);
			httpClientMock.Setup(x => x.GetStringAsync(
					It.Is<Uri>(u => u.AbsoluteUri.Equals("http://www.example.com/")),
					It.Is<CancellationToken>(t => t.Equals(CancellationToken.None)),
					It.Is<AuthenticationHeaderValue>(ahv => ahv.Scheme.Equals("Bearer") && ahv.Parameter.Equals("token")),
					It.Is<MediaTypeWithQualityHeaderValue>(m => m.MediaType.Equals("application/json"))))
				.Throws(new HttpRequestException());

			var apiService = GetService(() => httpClientMock.Object);

			Assert.ThrowsAsync<LtiException>(
				Task.Run(async () =>
				{
					await apiService.GetResultAsync<JObject>(_ltiExternalProvider, "http://www.example.com",
						CancellationToken.None,
						new MediaTypeWithQualityHeaderValue("application/json"));
				}), Strings.LtiApiRequestError);
		}

		[TestMethod]
		public void GetResultsAsync_ShouldThrowOnInvalidJson()
		{
			var httpClientMock = new Mock<IHttpClient>();
			httpClientMock.Setup(x => x.PostAsync(
					It.Is<Uri>(u => u.AbsoluteUri.Equals(_ltiExternalProvider.OauthTokenUrl)),
					It.IsNotNull<Func<Uri, HttpContent>>(),
					It.IsAny<CancellationToken>(),
					It.IsAny<AuthenticationHeaderValue>(),
					It.IsAny<MediaTypeWithQualityHeaderValue>()))
				.ReturnsAsync(TestToken);
			httpClientMock.Setup(x => x.GetStringAsync(
					It.Is<Uri>(u => u.AbsoluteUri.Equals("http://www.example.com/")),
					It.Is<CancellationToken>(t => t.Equals(CancellationToken.None)),
					It.Is<AuthenticationHeaderValue>(ahv => ahv.Scheme.Equals("Bearer") && ahv.Parameter.Equals("token")),
					It.Is<MediaTypeWithQualityHeaderValue>(m => m.MediaType.Equals("application/json"))))
				.ReturnsAsync(invalidJsonResponse);

			var apiService = GetService(() => httpClientMock.Object);

			Assert.ThrowsAsync<LtiException>(
				Task.Run(async () =>
				{
					await apiService.GetResultAsync<JObject>(_ltiExternalProvider, "http://www.example.com",
						CancellationToken.None,
						new MediaTypeWithQualityHeaderValue("application/json"));
				}), Strings.LtiJsonError);
		}

		#endregion GetResultAsync

		#region PostAsync

		[TestMethod]
		public void PostAsync_ShouldThrowIfExternalProviderNull()
		{
			var apiService = GetService(() => new RetryingHttpClient());

			Assert.ThrowsAsync<ArgumentNullException>(
				Task.Run(async () =>
				{
					await apiService.PostAsync<JObject>(null, "http://www.example.com", uri => new StringContent(""),
						CancellationToken.None);
				}), $"Value cannot be null.{Environment.NewLine}Parameter name: ltiExternalProvider");
		}

		[TestMethod]
		public void PostAsync_ShouldThrowIfUrlNull()
		{
			var apiService = GetService(() => new RetryingHttpClient());

			Assert.ThrowsAsync<ArgumentNullException>(
				Task.Run(async () =>
					{
						await apiService.PostAsync<JObject>(_ltiExternalProvider, null, uri => new StringContent(""),
							CancellationToken.None);
					}), $"Value cannot be null.{Environment.NewLine}Parameter name: url");
		}

		[TestMethod]
		public void PostAsync_ShouldThrowIfContentNull()
		{
			var apiService = GetService(() => new RetryingHttpClient());

			Assert.ThrowsAsync<ArgumentNullException>(
				Task.Run(async () =>
				{
					await apiService.PostAsync<JObject>(_ltiExternalProvider, "http://www.example.com", null, CancellationToken.None);
				}), $"Value cannot be null.{Environment.NewLine}Parameter name: getHttpContent");
		}

		[TestMethod]
		public async Task PostAsync_ShouldSucceed()
		{
			var httpClientMock = new Mock<IHttpClient>();
			httpClientMock.Setup(x => x.PostAsync(
					It.Is<Uri>(u => u.AbsoluteUri.Equals(_ltiExternalProvider.OauthTokenUrl)),
					It.IsNotNull<Func<Uri, HttpContent>>(),
					It.IsAny<CancellationToken>(),
					It.IsAny<AuthenticationHeaderValue>(),
					It.IsAny<MediaTypeWithQualityHeaderValue>()))
				.ReturnsAsync(TestToken);
			httpClientMock.Setup(x => x.PostAsync(
					It.Is<Uri>(u => u.AbsoluteUri.Equals("http://www.example.com/")),
					It.IsNotNull<Func<Uri, HttpContent>>(),
					It.IsNotNull<CancellationToken>(),
					It.Is<AuthenticationHeaderValue>(ahv => ahv.Scheme.Equals("Bearer") && ahv.Parameter.Equals("token")),
					It.Is<MediaTypeWithQualityHeaderValue>(hv => hv.MediaType.Equals("application/json"))))
				.ReturnsAsync("{\"test\":\"json\"}");

			var apiService = GetService(() => httpClientMock.Object);
			var result = await apiService.PostAsync<JObject>(_ltiExternalProvider, "http://www.example.com", uri => new StringContent(""),
				CancellationToken.None, new MediaTypeWithQualityHeaderValue("application/json"));

			Assert.AreEqual("json", result.Value<string>("test"));
		}

		[TestMethod]
		public void PostAsync_ShouldThrowOnNetworkError()
		{
			var httpClientMock = new Mock<IHttpClient>();
			httpClientMock.Setup(x => x.PostAsync(
					It.Is<Uri>(u => u.AbsoluteUri.Equals(_ltiExternalProvider.OauthTokenUrl)),
					It.IsNotNull<Func<Uri, HttpContent>>(),
					It.IsAny<CancellationToken>(),
					It.IsAny<AuthenticationHeaderValue>(),
					It.IsAny<MediaTypeWithQualityHeaderValue>()))
				.ReturnsAsync(TestToken);
			httpClientMock.Setup(x => x.PostAsync(
					It.Is<Uri>(u => u.AbsoluteUri.Equals("http://www.example.com/")),
					It.IsNotNull<Func<Uri, HttpContent>>(),
					It.IsNotNull<CancellationToken>(),
					It.Is<AuthenticationHeaderValue>(ahv => ahv.Scheme.Equals("Bearer") && ahv.Parameter.Equals("token")),
					It.Is<MediaTypeWithQualityHeaderValue>(hv => hv.MediaType.Equals("application/json"))))
				.Throws(new HttpRequestException());

			var apiService = GetService(() => httpClientMock.Object);

			Assert.ThrowsAsync<LtiException>(
				Task.Run(async () =>
				{
					await apiService.PostAsync<JObject>(_ltiExternalProvider, "http://www.example.com", uri => new StringContent(""),
						CancellationToken.None, new MediaTypeWithQualityHeaderValue("application/json"));
				}), Strings.LtiApiRequestError);
		}

		[TestMethod]
		public void PostAsync_ShouldThrowOnInvalidJson()
		{
			var httpClientMock = new Mock<IHttpClient>();
			httpClientMock.Setup(x => x.PostAsync(
					It.Is<Uri>(u => u.AbsoluteUri.Equals(_ltiExternalProvider.OauthTokenUrl)),
					It.IsNotNull<Func<Uri, HttpContent>>(),
					It.IsAny<CancellationToken>(),
					It.IsAny<AuthenticationHeaderValue>(),
					It.IsAny<MediaTypeWithQualityHeaderValue>()))
				.ReturnsAsync(TestToken);
			httpClientMock.Setup(x => x.PostAsync(
					It.Is<Uri>(u => u.AbsoluteUri.Equals("http://www.example.com/")),
					It.IsNotNull<Func<Uri, HttpContent>>(),
					It.IsNotNull<CancellationToken>(),
					It.Is<AuthenticationHeaderValue>(ahv => ahv.Scheme.Equals("Bearer") && ahv.Parameter.Equals("token")),
					It.Is<MediaTypeWithQualityHeaderValue>(hv => hv.MediaType.Equals("application/json"))))
				.ReturnsAsync(invalidJsonResponse);

			var apiService = GetService(() => httpClientMock.Object);

			Assert.ThrowsAsync<LtiException>(
				Task.Run(async () =>
				{
					await apiService.PostAsync<JObject>(_ltiExternalProvider, "http://www.example.com", uri => new StringContent(""),
						CancellationToken.None, new MediaTypeWithQualityHeaderValue("application/json"));
				}), Strings.LtiJsonError);
		}

		#endregion PostAsync

		#region GetAuthTokenAsync

		[TestMethod]
		public void GetAuthTokenAsync_ShouldThrowIfProviderNull()
		{
			var apiService = GetService(() => new RetryingHttpClient());

			Assert.ThrowsAsync<ArgumentNullException>(
				Task.Run(async () =>
				{
					await apiService.GetAuthTokenAsync(null);
				}), $"Value cannot be null.{Environment.NewLine}Parameter name: provider");
		}

		[TestMethod]
		public async Task GetAuthTokenAsync_ShouldUseCache()
		{
			const string testToken2 =
				"{\"access_token\":\"token2\",\"token_type\":\"Bearer\",\"expires_in\":3600,\"scope\":\"https://purl.imsglobal.org/spec/lti-nrps/scope/contextmembership.readonly https://purl.imsglobal.org/spec/lti-ags/scope/lineitem https://purl.imsglobal.org/spec/lti-ags/scope/result.readonly https://purl.imsglobal.org/spec/lti-ags/scope/score\"}";

			var tokenStack = new Stack<string>();
			tokenStack.Push(testToken2);
			tokenStack.Push(TestToken);

			var httpClientMock = new Mock<IHttpClient>();
			httpClientMock.Setup(x => x.PostAsync(
					It.Is<Uri>(u => u.AbsoluteUri.Equals(_ltiExternalProvider.OauthTokenUrl)),
					It.IsNotNull<Func<Uri, HttpContent>>(),
					It.IsAny<CancellationToken>(),
					It.Is<AuthenticationHeaderValue>(ahv => ahv == null),
					It.Is<MediaTypeWithQualityHeaderValue>(hv => hv == null)))
				.ReturnsAsync(tokenStack.Pop);

			LtiApiService.ClearCache();
			var apiService = GetService(() => httpClientMock.Object);
			var result = await apiService.GetAuthTokenAsync(_ltiExternalProvider);
			var result2 = await apiService.GetAuthTokenAsync(_ltiExternalProvider);

			httpClientMock.Verify(c => c.PostAsync(
					It.Is<Uri>(u => u.AbsoluteUri.Equals(_ltiExternalProvider.OauthTokenUrl)),
					It.IsNotNull<Func<Uri, HttpContent>>(),
					It.IsAny<CancellationToken>(),
					It.Is<AuthenticationHeaderValue>(ahv => ahv == null),
					It.Is<MediaTypeWithQualityHeaderValue>(hv => hv == null)),
				Times.Once);

			Assert.AreEqual("token", result);
			Assert.AreEqual("token", result2);
		}

		[TestMethod]
		public async Task GetAuthTokenAsync_ShouldRenewIfTokenExpiring()
		{
			//Token refreshes if it expires within the next 5 minutes, this one only lasts 4 minutes
			var expiringToken =
				"{\"access_token\":\"expiringToken\",\"token_type\":\"Bearer\",\"expires_in\": 240,\"scope\":\"https://purl.imsglobal.org/spec/lti-nrps/scope/contextmembership.readonly https://purl.imsglobal.org/spec/lti-ags/scope/lineitem https://purl.imsglobal.org/spec/lti-ags/scope/result.readonly https://purl.imsglobal.org/spec/lti-ags/scope/score\"}";
			var tokenStack = new Stack<string>();
			tokenStack.Push(TestToken);
			tokenStack.Push(expiringToken);

			var httpClientMock = new Mock<IHttpClient>();
			httpClientMock.Setup(x => x.PostAsync(
					It.Is<Uri>(u => u.AbsoluteUri.Equals(_ltiExternalProvider.OauthTokenUrl)),
					It.IsNotNull<Func<Uri, HttpContent>>(),
					It.IsAny<CancellationToken>(),
					It.Is<AuthenticationHeaderValue>(ahv => ahv == null),
					It.Is<MediaTypeWithQualityHeaderValue>(hv => hv == null)))
				.ReturnsAsync(tokenStack.Pop);

			LtiApiService.ClearCache();
			var apiService = GetService(() => httpClientMock.Object);
			var result = await apiService.GetAuthTokenAsync(_ltiExternalProvider, CancellationToken.None);
			var result2 = await apiService.GetAuthTokenAsync(_ltiExternalProvider, CancellationToken.None);

			httpClientMock.Verify(c => c.PostAsync(
					It.Is<Uri>(u => u.AbsoluteUri.Equals(_ltiExternalProvider.OauthTokenUrl)),
					It.IsNotNull<Func<Uri, HttpContent>>(),
					It.IsAny<CancellationToken>(),
					It.Is<AuthenticationHeaderValue>(ahv => ahv == null),
					It.Is<MediaTypeWithQualityHeaderValue>(hv => hv == null)),
				Times.Exactly(2));

			Assert.AreEqual("expiringToken", result);
			Assert.AreEqual("token", result2);
		}

		[TestMethod]
		public void GetAuthTokenAsync_ShouldThrowOnNetworkException()
		{
			var httpClientMock = new Mock<IHttpClient>();
			httpClientMock.Setup(x => x.PostAsync(
					It.Is<Uri>(u => u.AbsoluteUri.Equals(_ltiExternalProvider.OauthTokenUrl)),
					It.IsNotNull<Func<Uri, HttpContent>>(),
					It.IsAny<CancellationToken>(),
					It.Is<AuthenticationHeaderValue>(ahv => ahv == null),
					It.Is<MediaTypeWithQualityHeaderValue>(hv => hv == null)))
				.Throws(new HttpRequestException());

			LtiApiService.ClearCache();
			var apiService = GetService(() => httpClientMock.Object);

			Assert.ThrowsAsync<LtiException>(
				Task.Run(async () =>
				{
					await apiService.GetAuthTokenAsync(_ltiExternalProvider);
				}), Strings.LtiOauthRequestError);
		}

		[TestMethod]
		public async Task GetAuthTokenAsync_ShouldReturnCachedTokenOnNetworkExceptionIfNotExpired()
		{
			const string expiringToken =
				"{\"access_token\":\"expiringToken\",\"token_type\":\"Bearer\",\"expires_in\": 240,\"scope\":\"https://purl.imsglobal.org/spec/lti-nrps/scope/contextmembership.readonly https://purl.imsglobal.org/spec/lti-ags/scope/lineitem https://purl.imsglobal.org/spec/lti-ags/scope/result.readonly https://purl.imsglobal.org/spec/lti-ags/scope/score\"}";
			var tokenStack = new Stack<string>();
			tokenStack.Push(TestToken);
			tokenStack.Push(expiringToken);

			var httpClientMock = new Mock<IHttpClient>();
			httpClientMock.Setup(x => x.PostAsync(
					It.Is<Uri>(u => u.AbsoluteUri.Equals(_ltiExternalProvider.OauthTokenUrl)),
					It.IsNotNull<Func<Uri, HttpContent>>(),
					It.IsAny<CancellationToken>(),
					It.Is<AuthenticationHeaderValue>(ahv => ahv == null),
					It.Is<MediaTypeWithQualityHeaderValue>(hv => hv == null)))
				.ReturnsAsync(() => tokenStack.Count == 2 ? tokenStack.Pop() : throw new HttpRequestException());

			LtiApiService.ClearCache();
			var apiService = GetService(() => httpClientMock.Object);
			var result = await apiService.GetAuthTokenAsync(_ltiExternalProvider);
			var result2 = await apiService.GetAuthTokenAsync(_ltiExternalProvider);

			Assert.AreEqual("expiringToken", result);
			Assert.AreEqual("expiringToken", result2);
		}

		[TestMethod]
		public async Task GetAuthTokenAsync_ShouldThrowOnNetworkErrorWithExpiredCacheToken()
		{
			// Token expired two minutes ago
			const string expiringToken =
				"{\"access_token\":\"expiringToken\",\"token_type\":\"Bearer\",\"expires_in\": -120,\"scope\":\"https://purl.imsglobal.org/spec/lti-nrps/scope/contextmembership.readonly https://purl.imsglobal.org/spec/lti-ags/scope/lineitem https://purl.imsglobal.org/spec/lti-ags/scope/result.readonly https://purl.imsglobal.org/spec/lti-ags/scope/score\"}";
			var tokenStack = new Stack<string>();
			tokenStack.Push(TestToken);
			tokenStack.Push(expiringToken);

			var httpClientMock = new Mock<IHttpClient>();
			httpClientMock.Setup(x => x.PostAsync(
					It.Is<Uri>(u => u.AbsoluteUri.Equals(_ltiExternalProvider.OauthTokenUrl)),
					It.IsNotNull<Func<Uri, HttpContent>>(),
					It.IsAny<CancellationToken>(),
					It.Is<AuthenticationHeaderValue>(ahv => ahv == null),
					It.Is<MediaTypeWithQualityHeaderValue>(hv => hv == null)))
				.ReturnsAsync(() => tokenStack.Count == 2 ? tokenStack.Pop() : throw new HttpRequestException());

			LtiApiService.ClearCache();
			var apiService = GetService(() => httpClientMock.Object);
			var result = await apiService.GetAuthTokenAsync(_ltiExternalProvider);

			Assert.AreEqual("expiringToken", result);
			Assert.ThrowsAsync<LtiException>(
				Task.Run(async () =>
				{
					await apiService.GetAuthTokenAsync(_ltiExternalProvider);
				}), Strings.LtiOauthRequestError);
		}

		[TestMethod]
		public void GetAuthTokenAsync_ShouldThrowOnInvalidJson()
		{
			var httpClientMock = new Mock<IHttpClient>();
			httpClientMock.Setup(x => x.PostAsync(
					It.Is<Uri>(u => u.AbsoluteUri.Equals(_ltiExternalProvider.OauthTokenUrl)),
					It.IsNotNull<Func<Uri, HttpContent>>(),
					It.IsAny<CancellationToken>(),
					It.Is<AuthenticationHeaderValue>(ahv => ahv == null),
					It.Is<MediaTypeWithQualityHeaderValue>(hv => hv == null)))
				.ReturnsAsync(invalidJsonResponse);

			LtiApiService.ClearCache();
			var apiService = GetService(() => httpClientMock.Object);

			Assert.ThrowsAsync<LtiException>(
				Task.Run(async () =>
				{
					await apiService.GetAuthTokenAsync(_ltiExternalProvider);
				}), Strings.LtiJsonError);
		}

		#endregion GetAuthTokenAsync

		#region Helpers

		private LtiApiService GetService(Func<IHttpClient> httpClientProviderFunc)
		{
			return new LtiApiService(_jwtTokenService, httpClientProviderFunc, _errorHandler);
		}

		#endregion Helpers
	}
}