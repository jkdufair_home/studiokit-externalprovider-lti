﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using MSTestExtensions;
using StudioKit.ExternalProvider.Lti.BusinessLogic.Services;
using StudioKit.ExternalProvider.Lti.Models;
using System;
using System.Threading.Tasks;

namespace StudioKit.ExternalProvider.Lti.Tests.BusinessLogic.Services
{
	[TestClass]
	public class LtiKeySetProviderServiceTests : BaseTest
	{
		#region GetKeySetAsync

		[TestMethod]
		public void GetKeySetAsync_ShouldThrowWithNullExternalProvider()
		{
			var service = GetService();
			Assert.ThrowsAsync<ArgumentNullException>(
				Task.Run(async () =>
				{
					await service.GetKeySetAsync(null);
				}), $"Value cannot be null.{Environment.NewLine}Parameter name: ltiExternalProvider");
		}

		[TestMethod]
		public void GetKeySetAsync_ShouldThrowWithNullKeysetUrl()
		{
			var service = GetService();
			Assert.ThrowsAsync<ArgumentNullException>(
				Task.Run(async () =>
				{
					await service.GetKeySetAsync(new LtiExternalProvider());
				}), $"Value cannot be null.{Environment.NewLine}Parameter name: {nameof(LtiExternalProvider.KeySetUrl)}");

			Assert.ThrowsAsync<ArgumentNullException>(
				Task.Run(async () =>
				{
					await service.GetKeySetAsync(new LtiExternalProvider { KeySetUrl = "" });
				}), $"Value cannot be null.{Environment.NewLine}Parameter name: {nameof(LtiExternalProvider.KeySetUrl)}");
		}

		#endregion GetKeySetAsync

		#region Helpers

		private static LtiKeySetProviderService GetService()
		{
			return new LtiKeySetProviderService();
			;
		}

		#endregion Helpers
	}
}