﻿using Effort;
using Microsoft.IdentityModel.Tokens;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using MSTestExtensions;
using StudioKit.ErrorHandling.Exceptions;
using StudioKit.ExternalProvider.Lti.BusinessLogic.Interfaces;
using StudioKit.ExternalProvider.Lti.BusinessLogic.Models;
using StudioKit.ExternalProvider.Lti.BusinessLogic.Services;
using StudioKit.ExternalProvider.Lti.Exceptions;
using StudioKit.ExternalProvider.Lti.Models;
using StudioKit.ExternalProvider.Lti.Properties;
using StudioKit.ExternalProvider.Lti.Tests.TestData;
using StudioKit.ExternalProvider.Lti.Tests.TestData.DataAccess;
using StudioKit.ExternalProvider.Tests.TestData.Models;
using StudioKit.Scaffolding.Models;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Threading;
using System.Threading.Tasks;

namespace StudioKit.ExternalProvider.Lti.Tests.BusinessLogic.Services
{
	[TestClass]
	public class JwtTokenServiceTests : BaseTest
	{
		private SigningCredentials _signingCredentials;
		private JsonWebKeySet _jsonWebKeySet;
		private ILtiKeySetProviderService _keySetProviderService;
		private ILtiConfigurationProvider _configurationProvider;

		private readonly LtiExternalProvider _ltiExternalProvider = new LtiExternalProvider()
		{
			Name = "Lti Unit Test Provider",
			Issuer = TestUtils.Issuer,
			OauthClientId = TestUtils.Audience,
			DeploymentId = TestUtils.DeploymentId.ToString(),
			OauthTokenUrl = "dummy",
			KeySetUrl = "dummy",
			DateStored = DateTime.UtcNow,
			DateLastUpdated = DateTime.UtcNow
		};

		[TestInitialize]
		public void Initialize()
		{
			var (signingCredentials, jsonWebKeySet) = TestUtils.GetCredentials();
			var privateKeyBytes = TestUtils.GetPrivateKeyBytes();
			_signingCredentials = signingCredentials;
			_jsonWebKeySet = jsonWebKeySet;

			var mockKeySetProviderService = new Mock<ILtiKeySetProviderService>();
			mockKeySetProviderService.Setup(x => x.GetKeySetAsync(It.IsAny<LtiExternalProvider>(), It.IsAny<CancellationToken>())).ReturnsAsync(_jsonWebKeySet);
			_keySetProviderService = mockKeySetProviderService.Object;

			var ltiConfiguration = new BaseConfiguration
			{
				PrivateRsaKey = privateKeyBytes
			};

			var mockConfigurationProvider = new Mock<ILtiConfigurationProvider>();
			mockConfigurationProvider.Setup(x => x.GetLtiConfigurationAsync(It.IsAny<CancellationToken>())).ReturnsAsync(ltiConfiguration);
			_configurationProvider = mockConfigurationProvider.Object;
		}

		#region GetTokenProviderAsync

		[TestMethod]
		public void GetTokenProviderAsync_ShouldThrowIfTokenIsNull()
		{
			using (var dbContext = new TestLtiLaunchDbContext(DbConnectionFactory.CreateTransient()))
			{
				var service = GetService(dbContext);
				Assert.ThrowsAsync<ArgumentNullException>(
					Task.Run(async () =>
					{
						await service.GetTokenProviderAsync(null);
					}), $"Value cannot be null.{Environment.NewLine}Parameter name: token");
			}
		}

		[TestMethod]
		public async Task RetrieveTokenProviderAsync_ShouldReturnNullIfNoProvidersInDatabase()
		{
			using (var dbContext = new TestLtiLaunchDbContext(DbConnectionFactory.CreateTransient()))
			{
				var service = GetService(dbContext);
				var token = TestUtils.GetToken(_signingCredentials);
				var externalProvider = await service.GetTokenProviderAsync(token);
				Assert.IsNull(externalProvider);
			}
		}

		[TestMethod]
		public async Task RetrieveTokenProviderAsync_ShouldReturnNullIfNoDeploymentIdClaim()
		{
			using (var dbContext = new TestLtiLaunchDbContext(DbConnectionFactory.CreateTransient()))
			{
				await SeedExternalProviderAsync(dbContext);
				var service = GetService(dbContext);
				var claimsDictionary = TestUtils.GetClaimsDictionary();
				claimsDictionary.Remove(LtiClaim.DeploymentId);
				var token = TestUtils.GetToken(_signingCredentials, TestUtils.Issuer, TestUtils.Audience, TestUtils.GetClaims(claimsDictionary));
				var externalProvider = await service.GetTokenProviderAsync(token);
				Assert.IsNull(externalProvider);
			}
		}

		[TestMethod]
		public async Task RetrieveTokenProviderAsync_ShouldReturnNullIfDeploymentIdClaimDoesNotMatch()
		{
			using (var dbContext = new TestLtiLaunchDbContext(DbConnectionFactory.CreateTransient()))
			{
				await SeedExternalProviderAsync(dbContext);
				var service = GetService(dbContext);
				var claimsDictionary = TestUtils.GetClaimsDictionary();
				claimsDictionary[LtiClaim.DeploymentId] = "2";
				var token = TestUtils.GetToken(_signingCredentials, TestUtils.Issuer, TestUtils.Audience, TestUtils.GetClaims(claimsDictionary));
				var externalProvider = await service.GetTokenProviderAsync(token);
				Assert.IsNull(externalProvider);
			}
		}

		[TestMethod]
		public async Task RetrieveTokenProviderAsync_ShouldReturnNullIfAudienceClaimDoesNotMatch()
		{
			using (var dbContext = new TestLtiLaunchDbContext(DbConnectionFactory.CreateTransient()))
			{
				await SeedExternalProviderAsync(dbContext);
				var service = GetService(dbContext);
				var claimsDictionary = TestUtils.GetClaimsDictionary();
				claimsDictionary[LtiClaim.Audience] = "6789";
				var token = TestUtils.GetToken(_signingCredentials, TestUtils.Issuer, "6789", TestUtils.GetClaims(claimsDictionary));
				var externalProvider = await service.GetTokenProviderAsync(token);
				Assert.IsNull(externalProvider);
			}
		}

		#endregion GetTokenProviderAsync

		#region AssertIsTokenValidAsync

		[TestMethod]
		public void AssertIsTokenValidAsync_ShouldThrowIfNullTokenString()
		{
			using (var dbContext = new TestLtiLaunchDbContext(DbConnectionFactory.CreateTransient()))
			{
				var service = GetService(dbContext);
				Assert.ThrowsAsync<ArgumentNullException>(
					Task.Run(async () =>
					{
						await service.AssertIsTokenValidAsync(null);
					}), $"Value cannot be null.{Environment.NewLine}Parameter name: tokenString");

				Assert.ThrowsAsync<ArgumentNullException>(
					Task.Run(async () =>
					{
						await service.AssertIsTokenValidAsync(null);
					}), $"Value cannot be null.{Environment.NewLine}Parameter name: tokenString");
			}
		}

		[TestMethod]
		public async Task AssertIsTokenValidAsync_ShouldThrowForMissingClaims()
		{
			using (var dbContext = new TestLtiLaunchDbContext(DbConnectionFactory.CreateTransient()))
			{
				await SeedExternalProviderAsync(dbContext);
				var service = GetService(dbContext);

				Assert.ThrowsAsync<UnauthorizedException>(
					Task.Run(async () =>
					{
						var token = TestUtils.GetToken(_signingCredentials, null);
						await service.AssertIsTokenValidAsync(TestUtils.GetTokenString(token));
					}), string.Format(Strings.TokenMissingRequiredClaim, LtiClaim.Issuer));

				Assert.ThrowsAsync<UnauthorizedException>(
					Task.Run(async () =>
					{
						var token = TestUtils.GetToken(_signingCredentials, TestUtils.Issuer, null);
						await service.AssertIsTokenValidAsync(TestUtils.GetTokenString(token));
					}), string.Format(Strings.TokenMissingRequiredClaim, LtiClaim.Audience));

				Assert.ThrowsAsync<UnauthorizedException>(
					Task.Run(async () =>
					{
						var claimsDictionary = TestUtils.GetClaimsDictionary();
						claimsDictionary.Remove(LtiClaim.Subject);
						var token = TestUtils.GetToken(_signingCredentials, TestUtils.Issuer, TestUtils.Audience, TestUtils.GetClaims(claimsDictionary));
						await service.AssertIsTokenValidAsync(TestUtils.GetTokenString(token));
					}), string.Format(Strings.TokenMissingRequiredClaim, LtiClaim.Subject));

				Assert.ThrowsAsync<UnauthorizedException>(
					Task.Run(async () =>
					{
						var claimsDictionary = TestUtils.GetClaimsDictionary();
						claimsDictionary.Remove(LtiClaim.IssuedAt);
						var token = new JwtSecurityToken(TestUtils.Issuer, TestUtils.Audience, TestUtils.GetClaims(claimsDictionary), null, DateTime.UtcNow.AddHours(1), _signingCredentials);
						await service.AssertIsTokenValidAsync(TestUtils.GetTokenString(token));
					}), string.Format(Strings.TokenMissingRequiredClaim, LtiClaim.IssuedAt));

				Assert.ThrowsAsync<UnauthorizedException>(
					Task.Run(async () =>
					{
						var claimsDictionary = TestUtils.GetClaimsDictionary();
						claimsDictionary.Remove(LtiClaim.Expiration);
						var token = new JwtSecurityToken(TestUtils.Issuer, TestUtils.Audience, TestUtils.GetClaims(claimsDictionary), DateTime.UtcNow, null, _signingCredentials);
						await service.AssertIsTokenValidAsync(TestUtils.GetTokenString(token));
					}), string.Format(Strings.TokenMissingRequiredClaim, LtiClaim.Expiration));

				Assert.ThrowsAsync<UnauthorizedException>(
					Task.Run(async () =>
					{
						var claimsDictionary = TestUtils.GetClaimsDictionary();
						claimsDictionary.Remove(LtiClaim.Nonce);
						var token = TestUtils.GetToken(_signingCredentials, TestUtils.Issuer, TestUtils.Audience, TestUtils.GetClaims(claimsDictionary));
						await service.AssertIsTokenValidAsync(TestUtils.GetTokenString(token));
					}), string.Format(Strings.TokenMissingRequiredClaim, LtiClaim.Nonce));

				Assert.ThrowsAsync<UnauthorizedException>(
					Task.Run(async () =>
					{
						var claimsDictionary = TestUtils.GetClaimsDictionary();
						claimsDictionary.Remove(LtiClaim.DeploymentId);
						var token = TestUtils.GetToken(_signingCredentials, TestUtils.Issuer, TestUtils.Audience, TestUtils.GetClaims(claimsDictionary));
						await service.AssertIsTokenValidAsync(TestUtils.GetTokenString(token));
					}), string.Format(Strings.TokenMissingRequiredClaim, LtiClaim.DeploymentId));
			}
		}

		[TestMethod]
		public async Task AssertIsTokenValidAsync_ShouldThrowForMoreThanOneAudience()
		{
			using (var dbContext = new TestLtiLaunchDbContext(DbConnectionFactory.CreateTransient()))
			{
				await SeedExternalProviderAsync(dbContext);
				var service = GetService(dbContext);

				Assert.ThrowsAsync<UnauthorizedException>(
					Task.Run(async () =>
					{
						var claimsDictionary = TestUtils.GetClaimsDictionary();
						claimsDictionary[LtiClaim.Audience] = new[] { TestUtils.Audience, "5678" };
						var token = TestUtils.GetToken(_signingCredentials, TestUtils.Issuer, TestUtils.Audience, TestUtils.GetClaims(claimsDictionary));
						await service.AssertIsTokenValidAsync(TestUtils.GetTokenString(token));
					}), Strings.TokenUntrustedAudiences);
			}
		}

		[TestMethod]
		public void AssertIsTokenValidAsync_ShouldThrowIfProviderNotRegistered()
		{
			using (var dbContext = new TestLtiLaunchDbContext(DbConnectionFactory.CreateTransient()))
			{
				var service = GetService(dbContext);

				Assert.ThrowsAsync<UnauthorizedException>(
					Task.Run(async () =>
					{
						var token = TestUtils.GetToken(_signingCredentials);
						await service.AssertIsTokenValidAsync(TestUtils.GetTokenString(token));
					}), Strings.AudienceDeploymentIdNotRegistered);
			}
		}

		[TestMethod]
		public async Task AssertIsTokenValidAsync_ShouldThrowIfAuthorizedPartyClaimDoesNotMatchClientId()
		{
			using (var dbContext = new TestLtiLaunchDbContext(DbConnectionFactory.CreateTransient()))
			{
				await SeedExternalProviderAsync(dbContext);
				var service = GetService(dbContext);

				Assert.ThrowsAsync<UnauthorizedException>(
					Task.Run(async () =>
					{
						var claimsDictionary = TestUtils.GetClaimsDictionary();
						claimsDictionary[LtiClaim.AuthorizedParty] = "notTheRightValue";
						var token = TestUtils.GetToken(_signingCredentials, TestUtils.Issuer, TestUtils.Audience, TestUtils.GetClaims(claimsDictionary));
						await service.AssertIsTokenValidAsync(TestUtils.GetTokenString(token));
					}), Strings.AzpClaimDoesNotMatchClientId);
			}
		}

		[TestMethod]
		public async Task AssertIsTokenValidAsync_ShouldThrowIfTokenIsExpired()
		{
			using (var dbContext = new TestLtiLaunchDbContext(DbConnectionFactory.CreateTransient()))
			{
				await SeedExternalProviderAsync(dbContext);
				var service = GetService(dbContext);

				Assert.ThrowsAsync<UnauthorizedException>(
					Task.Run(async () =>
					{
						var issuedAt = DateTime.UtcNow.AddHours(-2);
						var expires = DateTime.UtcNow.AddHours(-1);
						var claimsDictionary = TestUtils.GetClaimsDictionary(issuedAt, expires);
						var token = TestUtils.GetToken(_signingCredentials, TestUtils.Issuer, TestUtils.Audience, TestUtils.GetClaims(claimsDictionary), issuedAt, expires);
						await service.AssertIsTokenValidAsync(TestUtils.GetTokenString(token));
					}), Strings.ValidateTokenFailed);
			}
		}

		[TestMethod]
		public async Task AssertIsTokenValidAsync_ShouldSucceedWithValidToken()
		{
			using (var dbContext = new TestLtiLaunchDbContext(DbConnectionFactory.CreateTransient()))
			{
				await SeedExternalProviderAsync(dbContext);
				var service = GetService(dbContext);
				var token = TestUtils.GetToken(_signingCredentials);
				await service.AssertIsTokenValidAsync(TestUtils.GetTokenString(token));
			}
		}

		#endregion AssertIsTokenValidAsync

		#region CreateJwtAsync

		[TestMethod]
		public void CreateJwtAsync_ShouldThrowIfProviderNull()
		{
			using (var dbContext = new TestLtiLaunchDbContext(DbConnectionFactory.CreateTransient()))
			{
				var service = GetService(dbContext);
				var claimList = new List<Claim>
				{
					new Claim("type1", "value1")
				};

				Assert.ThrowsAsync<ArgumentNullException>(
					Task.Run(async () =>
					{
						await service.CreateJwtAsync(null, claimList);
					}), $"Value cannot be null.{Environment.NewLine}Parameter name: provider"
				);
			}
		}

		[TestMethod]
		public async Task CreateJwtAsync_ShouldSucceedIfClaimsNull()
		{
			using (var dbContext = new TestLtiLaunchDbContext(DbConnectionFactory.CreateTransient()))
			{
				await SeedExternalProviderAsync(dbContext);
				var service = GetService(dbContext);

				var token = await service.CreateJwtAsync(_ltiExternalProvider);

				//Serializing should not throw an exception
				var jwtHandler = new JwtSecurityTokenHandler();
				jwtHandler.WriteToken(token);

				//Check the token contains the required Claims for LTI
				Assert.AreEqual(_ltiExternalProvider.OauthClientId, token.Issuer);
				Assert.AreEqual(1, token.Audiences.Count());
				Assert.AreEqual(_ltiExternalProvider.Issuer, token.Audiences.First());
				Assert.AreNotEqual(DateTime.MinValue, token.ValidTo); // 'exp' claim
				Assert.AreEqual(1, token.Claims.Count(c => c.Type.Equals(LtiClaim.IssuedAt) && c.Value != null));
				Assert.AreEqual(1, token.Claims.Count(c => c.Type.Equals(LtiClaim.Nonce) && c.Value != null));
			}
		}

		[TestMethod]
		public async Task CreateJwtAsync_ShouldSucceedAndIncludeLtiClaims()
		{
			using (var dbContext = new TestLtiLaunchDbContext(DbConnectionFactory.CreateTransient()))
			{
				await SeedExternalProviderAsync(dbContext);
				var service = GetService(dbContext);
				var claimList = new List<Claim>
				{
					new Claim("type1", "value1"),
					new Claim("type2", "value2")
				};
				var token = await service.CreateJwtAsync(_ltiExternalProvider, claimList);

				//Serializing should not throw an exception
				var jwtHandler = new JwtSecurityTokenHandler();
				jwtHandler.WriteToken(token);

				//Should contain the passed in claim
				Assert.AreEqual(1, token.Claims.Count(c => c.Type.Equals("type1") && c.Value.Equals("value1")));
				Assert.AreEqual(1, token.Claims.Count(c => c.Type.Equals("type2") && c.Value.Equals("value2")));
			}
		}

		[TestMethod]
		public async Task CreateJwtAsync_ShouldThrowIfCustomIssuerInvalid()
		{
			using (var dbContext = new TestLtiLaunchDbContext(DbConnectionFactory.CreateTransient()))
			{
				await SeedExternalProviderAsync(dbContext);
				var service = GetService(dbContext);

				string issuerValue = "invalidIssuer";
				var claimList = new List<Claim>
				{
					new Claim(LtiClaim.Issuer, issuerValue)
				};

				Assert.ThrowsAsync<LtiInvalidJwtException>(
					Task.Run(async () =>
					{
						await service.CreateJwtAsync(_ltiExternalProvider, claimList);
					}), $"Invalid claim {LtiClaim.Issuer} with value {issuerValue}"
				);
			}
		}

		public async Task CreateJwtAsync_ShouldNotOverrideOrDuplicateRequiredClaims()

		{
			using (var dbContext = new TestLtiLaunchDbContext(DbConnectionFactory.CreateTransient()))
			{
				await SeedExternalProviderAsync(dbContext);
				var service = GetService(dbContext);

				string issuerValue = _ltiExternalProvider.OauthClientId;
				const string audienceValue = "audience";
				const string nonceValue = "nonce-1234";
				var dateValue = DateTime.UtcNow.AddDays(-5);
				var issuedAtTimeSpan = dateValue.Subtract(new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc));
				var issuedAtValue = Convert.ToInt64(issuedAtTimeSpan.TotalSeconds).ToString();
				var expirationTimeSpan = dateValue.AddMinutes(3).Subtract(new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc));
				var expirationValue = Convert.ToInt64(expirationTimeSpan.TotalSeconds).ToString();
				var claimList = new List<Claim>
				{
					new Claim(LtiClaim.Issuer, issuerValue),
					new Claim(LtiClaim.Audience, audienceValue),
					new Claim(LtiClaim.Expiration, expirationValue),
					new Claim(LtiClaim.IssuedAt, issuedAtValue),
					new Claim(LtiClaim.Nonce, nonceValue)
				};

				var token = await service.CreateJwtAsync(_ltiExternalProvider, claimList);

				Assert.AreEqual(1, token.Claims.Count(c => c.Type.Equals(LtiClaim.Issuer)));
				Assert.AreEqual(issuerValue, token.Issuer);
				Assert.AreEqual(1, token.Audiences.Count());
				Assert.AreEqual(audienceValue, token.Audiences.Single());
				Assert.AreEqual(1, token.Claims.Count(c => c.Type.Equals(LtiClaim.Expiration)));
				Assert.AreEqual(expirationValue, token.Claims.Single(c => c.Type.Equals(LtiClaim.Expiration)).Value);
				Assert.AreEqual(1, token.Claims.Count(c => c.Type.Equals(LtiClaim.IssuedAt)));
				Assert.AreEqual(issuedAtValue, token.Claims.Single(c => c.Type.Equals(LtiClaim.IssuedAt)).Value);
				Assert.AreEqual(1, token.Claims.Count(c => c.Type.Equals(LtiClaim.Nonce)));
				Assert.AreEqual(nonceValue, token.Claims.Single(c => c.Type.Equals(LtiClaim.Nonce)).Value);
			}
		}

		#endregion CreateJwtAsync

		#region Helpers

		private JwtTokenService<TestGroupUserRole, TestGroupUserRoleLog, TestExternalGroup, TestExternalGroupUser> GetService(TestLtiLaunchDbContext dbContext)
		{
			return new JwtTokenService<TestGroupUserRole, TestGroupUserRoleLog, TestExternalGroup, TestExternalGroupUser>(
				dbContext, _keySetProviderService, _configurationProvider);
		}

		private async Task SeedExternalProviderAsync(TestLtiLaunchDbContext dbContext, CancellationToken cancellationToken = default(CancellationToken))
		{
			dbContext.LtiExternalProviders.Add(_ltiExternalProvider);
			await dbContext.SaveChangesAsync(cancellationToken);
		}

		#endregion Helpers
	}
}