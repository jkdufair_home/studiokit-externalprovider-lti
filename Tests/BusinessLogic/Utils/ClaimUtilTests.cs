﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using MSTestExtensions;
using StudioKit.Data.Entity.Identity;
using StudioKit.ExternalProvider.Lti.BusinessLogic.Models;
using StudioKit.ExternalProvider.Lti.BusinessLogic.Utils;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;

namespace StudioKit.ExternalProvider.Lti.Tests.BusinessLogic.Utils
{
	[TestClass]
	public class ClaimUtilTests : BaseTest
	{
		#region UpdateExternalClaimsIssuer

		[TestMethod]
		public void UpdateExternalClaimsIssuer_ShouldUpdateIssuers()
		{
			var resourceClaim = new Claim(LtiClaim.ResourceLink, "{\"id\": \"200d101f-2c14-434a-a0f3-57c2a42369fd\",\"description\": \"Assignment to introduce who you are\",\"title\": \"Introduction Assignment\"}");
			const string resourceId = "200d101f-2c14-434a-a0f3-57c2a42369fd";
			var contextClaim = new Claim(LtiClaim.Context, "{\"id\": \"c1d887f0-a1a3-4bca-ae25-c375edcc131a\",\"label\": \"ECON 1010\",\"title\": \"Economics as a Social Science\",\"type\": [\"http://purl.imsglobal.org/vocab/lis/v2/course#CourseOffering\"]}");
			const string contextId = "c1d887f0-a1a3-4bca-ae25-c375edcc131a";

			//Defaults to the same value for all claims
			var issuer = resourceClaim.Issuer;

			var claims = new List<Claim>
			{
				resourceClaim,
				contextClaim
			};

			var updatedIssuerClaims = ClaimUtils.UpdateExternalClaimsIssuer(claims);

			Assert.IsTrue(updatedIssuerClaims.All(c => c.Issuer.Equals($"{issuer}-{resourceId}-{contextId}")));
		}

		#endregion UpdateExternalClaimsIssuer

		#region GetExternalRoleNames

		[TestMethod]
		public void GetExternalRoleNames_ShouldReturnRoleValues()
		{
			var roleClaims = new List<Claim>
			{
				new Claim(LtiClaim.Roles, LtiRole.MembershipInstructor),
				new Claim(LtiClaim.Roles, LtiRole.MembershipAdministrator),
				new Claim(LtiClaim.Roles, LtiRole.Learner),
				new Claim(LtiClaim.Roles, LtiRole.SystemAdministrator),
				new Claim(LtiClaim.Roles, LtiRole.InstitutionStaff)
			};

			var externalRoleNames = ClaimUtils.GetExternalRoleNames(roleClaims);

			var expectedRoleNames = new List<string>
			{
				LtiRole.MembershipInstructor,
				LtiRole.MembershipAdministrator,
				LtiRole.Learner,
				LtiRole.SystemAdministrator,
				LtiRole.InstitutionStaff
			};

			externalRoleNames.Sort();
			expectedRoleNames.Sort();

			Assert.IsTrue(externalRoleNames.SequenceEqual(expectedRoleNames));
		}

		#endregion GetExternalRoleNames

		#region GetGroupRoleNames

		[TestMethod]
		public void GetGroupRoleNames_ShouldReturnGroupOwner()
		{
			var roleClaim = new Claim(LtiClaim.Roles, LtiRole.MembershipInstructor);
			var groupRoleNames = ClaimUtils.GetGroupRoleNames(new List<Claim> { roleClaim });
			groupRoleNames.Sort();
			var expectedGroupRoleNames = new List<string>
			{
				BaseRole.GroupOwner
			};
			expectedGroupRoleNames.Sort();
			Assert.IsTrue(groupRoleNames.SequenceEqual(expectedGroupRoleNames));
		}

		[TestMethod]
		public void GetGroupRoleNames_ShouldReturnGroupLearner()
		{
			var roleClaim = new Claim(LtiClaim.Roles, LtiRole.MembershipLearner);
			var groupRoleNames = ClaimUtils.GetGroupRoleNames(new List<Claim> { roleClaim });
			groupRoleNames.Sort();
			var expectedGroupRoleNames = new List<string>
			{
				BaseRole.GroupLearner
			};
			expectedGroupRoleNames.Sort();
			Assert.IsTrue(groupRoleNames.SequenceEqual(expectedGroupRoleNames));
		}

		#endregion GetGroupRoleNames

		#region GetCommonClaims

		[TestMethod]
		public void GetCommonClaims_ShouldMapToCommonClaims()
		{
			var idClaim = new Claim(LtiClaim.Subject, "id123");
			var givenNameClaim = new Claim(LtiClaim.GivenName, "user");
			var familyNameClaim = new Claim(LtiClaim.FamilyName, "McUser");
			var emailClaim = new Claim(LtiClaim.Email, "user@example.com");
			var extraClaim = new Claim("NotCommonClaim", "some value");

			var claimList = new List<Claim>
			{
				idClaim,
				givenNameClaim,
				familyNameClaim,
				emailClaim,
				extraClaim
			};

			var commonClaims = ClaimUtils.GetCommonClaims(claimList);

			//Verify commonClaims does not contain the extra claim
			Assert.IsTrue(commonClaims.Count == LtiClaim.CommonClaimMap.Count);

			//Verify claim values mapped correctly
			Assert.IsNotNull(commonClaims.SingleOrDefault(c =>
				c.Type.Equals(ClaimTypes.NameIdentifier) &&
				c.Value.Equals(idClaim.Value) &&
				c.ValueType.Equals(idClaim.ValueType) &&
				Equals(c.Subject, idClaim.Subject) &&
				c.Issuer.Equals(idClaim.Issuer) &&
				c.OriginalIssuer.Equals(idClaim.OriginalIssuer))
			);

			Assert.IsNotNull(commonClaims.SingleOrDefault(c =>
				c.Type.Equals(ClaimTypes.GivenName) &&
				c.Value.Equals(givenNameClaim.Value) &&
				c.ValueType.Equals(givenNameClaim.ValueType) &&
				Equals(c.Subject, givenNameClaim.Subject) &&
				c.Issuer.Equals(givenNameClaim.Issuer) &&
				c.OriginalIssuer.Equals(givenNameClaim.OriginalIssuer))
			);

			Assert.IsNotNull(commonClaims.SingleOrDefault(c =>
				c.Type.Equals(ClaimTypes.Surname) &&
				c.Value.Equals(familyNameClaim.Value) &&
				c.ValueType.Equals(familyNameClaim.ValueType) &&
				Equals(c.Subject, familyNameClaim.Subject) &&
				c.Issuer.Equals(familyNameClaim.Issuer) &&
				c.OriginalIssuer.Equals(familyNameClaim.OriginalIssuer))
			);

			Assert.IsNotNull(commonClaims.SingleOrDefault(c =>
				c.Type.Equals(ClaimTypes.Email) &&
				c.Value.Equals(emailClaim.Value) &&
				c.ValueType.Equals(emailClaim.ValueType) &&
				Equals(c.Subject, emailClaim.Subject) &&
				c.Issuer.Equals(emailClaim.Issuer) &&
				c.OriginalIssuer.Equals(emailClaim.OriginalIssuer))
			);
		}

		#endregion GetCommonClaims

		#region GetApplicationRoleNames

		[TestMethod]
		public void GetApplicationRoleNames_ShouldReturnCreator()
		{
			var roleClaim = new Claim(LtiClaim.Roles, LtiRole.MembershipInstructor);
			var applicationRoleNames = ClaimUtils.GetApplicationRoleNames(new List<Claim> { roleClaim });
			applicationRoleNames.Sort();
			var expectedRoleNames = new List<string>
			{
				BaseRole.Creator
			};
			expectedRoleNames.Sort();
			Assert.IsTrue(applicationRoleNames.SequenceEqual(expectedRoleNames));
		}

		#endregion GetApplicationRoleNames

		#region GetApplicationRoleClaims

		[TestMethod]
		public void GetApplicationRoleClaims_ShouldReturnNewClaims()
		{
			var roleClaim = new Claim(LtiClaim.Roles, LtiRole.MembershipInstructor);
			var applicationRoleClaims = ClaimUtils.GetApplicationRoleClaims(new List<Claim> { roleClaim });
			Assert.AreEqual(1, applicationRoleClaims.Count);
			Assert.AreEqual(ClaimTypes.Role, applicationRoleClaims[0].Type);
			Assert.AreEqual(BaseRole.Creator, applicationRoleClaims[0].Value);
		}

		#endregion GetApplicationRoleClaims
	}
}