﻿using StudioKit.Data.Entity.Identity;
using StudioKit.Data.Entity.Identity.Models;
using StudioKit.ExternalProvider.Lti.DataAccess;
using StudioKit.ExternalProvider.Lti.Models;
using StudioKit.ExternalProvider.Tests.TestData.Models;
using System.Data.Common;
using System.Data.Entity;

namespace StudioKit.ExternalProvider.Lti.Tests.TestData.DataAccess
{
	public class TestLtiLaunchDbContext : UserIdentityDbContext<TestUser, IdentityProvider>,
		ILtiLaunchDbContext<TestGroupUserRole, TestGroupUserRoleLog, TestExternalGroup, TestExternalGroupUser>
	{
		public TestLtiLaunchDbContext()
		{
		}

		public TestLtiLaunchDbContext(DbConnection existingConnection) : base(existingConnection)
		{
		}

		public DbSet<LtiExternalProvider> LtiExternalProviders { get; set; }

		public DbSet<TestGroup> Groups { get; set; }

		public DbSet<TestExternalGroup> ExternalGroups { get; set; }

		public DbSet<TestExternalGroupUser> ExternalGroupUsers { get; set; }

		public DbSet<TestGroupUserRole> GroupUserRoles { get; set; }

		public DbSet<TestGroupUserRoleLog> GroupUserRoleLogs { get; set; }

		public DbSet<LtiLaunch> LtiLaunches { get; set; }
	}
}