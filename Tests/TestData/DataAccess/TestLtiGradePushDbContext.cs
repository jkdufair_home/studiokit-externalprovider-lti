﻿using StudioKit.Data.Entity.Identity;
using StudioKit.Data.Entity.Identity.Models;
using StudioKit.ExternalProvider.DataAccess.Interfaces;
using StudioKit.ExternalProvider.Lti.Models;
using StudioKit.ExternalProvider.Tests.TestData.Models;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.Entity;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace StudioKit.ExternalProvider.Lti.Tests.TestData.DataAccess
{
	public class TestLtiGradePushDbContext : UserIdentityDbContext<TestUser, IdentityProvider>,
		IGradePushDbContext<TestGroup, TestGroupUserRole, TestExternalGroup, TestExternalGroupUser, TestExternalGroupAssignment, TestGroupAssignment, TestAssignment, TestGroupAssignmentScore, TestGroupAssignmentJobLog>
	{
		public TestLtiGradePushDbContext()
		{
		}

		public TestLtiGradePushDbContext(DbConnection existingConnection) : base(existingConnection)
		{
		}

		public DbSet<ExternalProvider.Models.ExternalProvider> ExternalProviders { get; set; }

		public DbSet<LtiExternalProvider> LtiExternalProviders { get; set; }

		public DbSet<TestGroup> Groups { get; set; }

		public DbSet<TestExternalGroup> ExternalGroups { get; set; }

		public DbSet<TestExternalGroupUser> ExternalGroupUsers { get; set; }

		public DbSet<TestExternalGroupAssignment> ExternalGroupAssignments { get; set; }

		public DbSet<TestGroupAssignment> GroupAssignments { get; set; }

		public DbSet<TestAssignment> Assignments { get; set; }

		public DbSet<TestGroupAssignmentScore> GroupAssignmentScores { get; set; }

		public DbSet<TestGroupAssignmentJobLog> GroupAssignmentJobLogs { get; set; }

		public async Task<List<TestGroupAssignmentScore>> GetScoresAsync(int groupAssignmentId, CancellationToken cancellationToken = default(CancellationToken))
		{
			return await GroupAssignmentScores.Where(s => s.GroupAssignmentId.Equals(groupAssignmentId))
				.ToListAsync(cancellationToken);
		}
	}
}