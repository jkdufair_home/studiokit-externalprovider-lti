﻿using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using StudioKit.ExternalProvider.Lti.BusinessLogic.Models;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Security.Cryptography;

namespace StudioKit.ExternalProvider.Lti.Tests.TestData
{
	public static class TestUtils
	{
		public const string TestPrivateKey = @"-----BEGIN RSA PRIVATE KEY-----
MIIEogIBAAKCAQEAo0YiQdd3b84+HiEwUlN1jZoqn5iU0ePpCqvhvi6mhejXEsvl
lrEVLgtYfWpIeP2HF7C1m66s1ml0JKi7AFdANubVHUB31cc1RLnS0gEafC+P+phk
llPfC/Sb9oI0/P2qHERdbk2Atl6voz9m3BSyK0Dhkno6USCxLgOtA8QIBt2UTvwi
T4Am74UgbRidIRVSPXTAT47bMOdez4yrNhPI0olr9x8+fjUH7tHASrg7o18qg7TF
6boOtEucUi2TVOGm3ZEgfQkNqu7agYkI/oxaHDb3dqAxUZsHtgNwyViOUg0wmXDG
oSGBdvon+NemxDTQpjVkTYBnEIsxn5QzvPyIzQIDAQABAoIBADJITA6I49B77Kds
wyvnZgF44/2IiPRmwwM1Ue03ArOktG+meYtB9+rZNaRSEgWo1mzWT+elELdrfIg5
qnV9aVo/FozOqNkeY2pJ7AIesuBh6W5cdkXRiJRu/YUaLmVnXXcdGsT4e1YurNqS
kQPgH8qwTYfFuqX3cnlgSm0pdutd3fqOuBiknHrN0W0/wS14/oR1Y/bDvOpo7jDw
Pa7n3ZYcOFOUc7BQPwkhiHDG/aEJwXum32xMEpR/UcIkvtAB/vE4VkTmoZsbk8p3
MfJp7o6feedQZTcgIOD4fxrPg/LCYyb+YSbqb8QHPK6hdTqkin+sYqysYHZbG8Ig
v90B8iUCgYEAzt0wOfVuqhaWPlZg6F/vXhPNtOr4kvZrV3TBtigjs3vVHozRS/iY
MHOUiHhu0fEC9yslEwX2jkWeteCl/EeksbC6FZzo1Sm/VfmPX1WYM7HYr7Kzjwc3
hZTZm8OVEiKgk0StaQVYz7rbcKXdXh/DocZ0C/SEzuaLsf0kMtm6WNsCgYEAyg5b
tbnCelSpRVB6XHItBNLbgzqU5BY8WQP6eeD6nhJnWyeNPJ/ehwW6Vn/85e+BKusr
pZuCeQFHFTqJ0q6bDD6YD31LOb0AMfiIYJE/u+oBoVEOBihG+CpAxsMJyFYOKIuT
jttFthRctMn9RCnkje/9/sxXtBW1jLvpwbmLIXcCgYB2yvh94B6L2GqxI6OE7kqk
iNTRdzoGEzJzR55SP9y4nn01jQJIEVs8P7NlZ1ukfjYIwKMKuJrs+rf4lChdprrC
O82wyam/d7jj42tdAOdlkFTyGLoagbd1o5QPahJ6Fp6F06ONsr9ck16e4vErsywC
A9fyYZm+wxAnx0n5VaU4jwKBgF3CeeYC0+7GGGIUrSL2zFMfsULczThl5Qz9Xp5t
un3dVl6jJNPL74vCKax36ZedItgSlodbeRjDcgO0zT2ZTlNJPHB7mIurW0rU5BvD
asq9FJKRtStAR2Zi3PoeiQyQejNwSUGSGHAcVMSDsmWN9wOyKK+yjMQBCuTKUfdW
LOyTAoGAGxKqfuaJEWJxktGR2yB93PlK0vptBKIdlvxgEl3/HJmK+LSU2AIQtMTW
njggIW2o5HDGhqh8RSSk3hggKx7LVNbcQeNul8oE7eClQSERy0LjVwrMhb9TGCVD
4HphNIPTgDbnc/nBaZ6XUDBvsTR0CBIXvuVckBTk4+Eyb6VLG1M=
-----END RSA PRIVATE KEY-----
";

		public const string Issuer = "https://lti-ri.imsglobal.org/";

		public const string Audience = "12345";

		public const int DeploymentId = 1;

		public const string Email = "unit.tester@example.org";

		public static string Sub = Guid.NewGuid().ToString();

		public static string ContextId = "17";

		public static string ContextTitle = "Course 1";

		public static string ContextLabel = "Course 1 Label";

		public static string LaunchPresentationReturnUrl = "https://lti-ri.imsglobal.org/platforms/19/returns";

		public static string NamesRoleServiceMembershipsUrl = "https://lti-ri.imsglobal.org/platforms/19/contexts/17/memberships.json";

		public static string EndpointLineItems = "https://lti-ri.imsglobal.org/platforms/19/line_items";

		private static long ToUnixTime(DateTime dateTime)
		{
			return (int)dateTime.ToUniversalTime().Subtract(new DateTime(1970, 1, 1)).TotalSeconds;
		}

		public static Dictionary<string, object> GetClaimsDictionary(DateTime? issued = null, DateTime? expires = null) => new Dictionary<string, object>
		{
			{LtiClaim.MessageType, LtiMessageType.LtiResourceLinkRequest},
			{LtiClaim.IssuedAt, ToUnixTime(issued ?? DateTime.UtcNow)},
			{LtiClaim.Expiration, ToUnixTime(expires ?? DateTime.UtcNow.AddHours(1))},
			{LtiClaim.Subject, Sub},
			{LtiClaim.Nonce, "dummy"},
			{LtiClaim.DeploymentId, DeploymentId},
			{
				LtiClaim.Platform,
				new Dictionary<string, object> {
					{"name", "Test Platform"},
					{"contact_email", ""},
					{"description", ""},
					{"url", ""},
					{"product_family_code", ""},
					{"version", "1.0"}
				}
			},
			{LtiClaim.Version, LtiVersion.Lti1P3},
			{LtiClaim.Locale, "en-US"},
			{
				LtiClaim.LaunchPresentation,
				new Dictionary<string, object> {
					{"document_target", "iframe"},
					{"height", 320},
					{"width", 240},
					{"return_url", LaunchPresentationReturnUrl}
				}
			},
			{LtiClaim.GivenName, "Absolute"},
			{LtiClaim.FamilyName, "Unit"},
			{LtiClaim.MiddleName, "Testing"},
			{LtiClaim.Picture, "http://example.org/Carson.jpg"},
			{LtiClaim.Email, "unit.tester@example.org"},
			{LtiClaim.Name, "Absolute Testing Unit"},
			{
				LtiClaim.Roles,
				new [] {LtiRole.MembershipInstructor}
			},
			{
				LtiClaim.ResourceLink,
				new Dictionary<string, object> {
					{"id", "35"},
					{"title", "Test Link 1"},
					{"description", "First Test Link"}
				}
			},
			{
				LtiClaim.Context,
				new Dictionary<string, object> {
					{"id", ContextId},
					{"label", ContextLabel},
					{"title", ContextTitle},
					{"type", new []{"Course Section"}}
				}
			},
			{
				LtiAgsClaim.Endpoint,
				new Dictionary<string, object> {
					{
						"scope",
						new []{
							LtiAgsScope.LineItem,
							LtiAgsScope.ResultReadOnly,
							LtiAgsScope.Score
						}
					},
					{"lineitems", EndpointLineItems},
					{"lineitem", "https://lti-ri.imsglobal.org/platforms/19/line_items/23"}
				}
			},
			{
				LtiNrpsClaim.NamesRoleService,
				new Dictionary<string, object>
				{
					{"context_memberships_url", NamesRoleServiceMembershipsUrl},
					{"service_version", "2.0"}
				}
			},
			{
				LtiClaim.Custom,
				new Dictionary<string, object> {
					{"myCustomValue", 123}
				}
			},
			{
				"https://www.example.com/extension",
				new Dictionary<string, object> {
					{"color", "violet"}
				}
			}
		};

		public static IEnumerable<Claim> GetClaims(Dictionary<string, object> claimsDictionary)
		{
			return claimsDictionary
				.SelectMany(kvp =>
				{
					var rawValue = kvp.Value;
					switch (rawValue)
					{
						case string[] strings:
							return strings.Select(v => new Claim(kvp.Key, v, ClaimValueTypes.String, Issuer));

						case Dictionary<string, object> _:
							return new List<Claim>
							{
								new Claim(kvp.Key, JsonConvert.SerializeObject(rawValue), ClaimValueTypes.String, Issuer)
							};

						default:
							return new List<Claim>
							{
								new Claim(kvp.Key, rawValue.ToString(), ClaimValueTypes.String, Issuer)
							};
					}
				})
				.ToList();
		}

		public static (SigningCredentials, JsonWebKeySet) GetCredentials()
		{
			var rsa = new RSACryptoServiceProvider(2048); // Generate a new 2048 bit RSA key
			var signingCredentials = new SigningCredentials(new RsaSecurityKey(rsa), SecurityAlgorithms.RsaSha256Signature, SecurityAlgorithms.Sha256Digest);
			var rsaParameters = rsa.ExportParameters(false);
			var jsonWebKeySet = GetWebKeySet(rsaParameters);
			return (signingCredentials, jsonWebKeySet);
		}

		public static byte[] GetPrivateKeyBytes()
		{
			return System.Text.Encoding.UTF8.GetBytes(TestPrivateKey);
		}

		/// <summary>
		/// https://gist.github.com/nanasess/ff8629c476edfddc1b304f9759b9eaa3
		/// </summary>
		/// <param name="keyParameters"></param>
		/// <returns></returns>
		public static JsonWebKeySet GetWebKeySet(RSAParameters keyParameters)
		{
			var e = Base64UrlEncoder.Encode(keyParameters.Exponent);
			var n = Base64UrlEncoder.Encode(keyParameters.Modulus);
			var dict = new Dictionary<string, string> {
				{"e", e},
				{"kty", "RSA"},
				{"n", n}
			};
			var hash = SHA256.Create();
			var hashBytes = hash.ComputeHash(System.Text.Encoding.ASCII.GetBytes(JsonConvert.SerializeObject(dict)));
			var jsonWebKey = new JsonWebKey
			{
				Kid = Base64UrlEncoder.Encode(hashBytes),
				Kty = "RSA",
				E = e,
				N = n
			};
			var jsonWebKeySet = new JsonWebKeySet();
			jsonWebKeySet.Keys.Add(jsonWebKey);
			return jsonWebKeySet;
		}

		public static JwtSecurityToken GetToken(SigningCredentials signingCredentials, string issuer = Issuer, string audience = Audience, IEnumerable<Claim> claims = null, DateTime? issued = null, DateTime? expires = null)
		{
			var issuedValue = issued ?? DateTime.UtcNow;
			var expiresValue = expires ?? DateTime.UtcNow.AddHours(1);
			var claimsValue = claims ?? GetClaims(GetClaimsDictionary(issuedValue, expiresValue));
			return new JwtSecurityToken(issuer, audience, claimsValue, issuedValue, expiresValue, signingCredentials);
		}

		public static string GetTokenString(JwtSecurityToken token)
		{
			var handler = new JwtSecurityTokenHandler();
			return handler.WriteToken(token);
		}
	}
}