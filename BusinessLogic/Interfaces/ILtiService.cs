﻿using Microsoft.AspNet.Identity.Owin;
using StudioKit.ExternalProvider.Lti.BusinessLogic.Models;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading;
using System.Threading.Tasks;

namespace StudioKit.ExternalProvider.Lti.BusinessLogic.Interfaces
{
	public interface ILtiService
	{
		string GetLaunchReturnUrl(string token);

		Task<ExternalLoginInfo> GetLoginInfoAsync(string token, CancellationToken cancellationToken = default(CancellationToken));

		Task<LaunchResponse> GetLaunchResponseAsync(string token, string userId, List<Claim> claims, CancellationToken cancellationToken = default(CancellationToken));
	}
}