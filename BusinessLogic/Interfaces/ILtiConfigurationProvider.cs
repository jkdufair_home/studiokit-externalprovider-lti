﻿using System.Threading;
using StudioKit.ExternalProvider.Lti.Models.Interfaces;
using System.Threading.Tasks;

namespace StudioKit.ExternalProvider.Lti.BusinessLogic.Interfaces
{
	public interface ILtiConfigurationProvider
	{
		Task<ILtiConfiguration> GetLtiConfigurationAsync(CancellationToken cancellationToken = default(CancellationToken));
	}
}