﻿using System.Security.Principal;
using System.Threading;
using System.Threading.Tasks;

namespace StudioKit.ExternalProvider.Lti.BusinessLogic.Interfaces
{
	public interface ILtiGradePushService
	{
		Task PushGroupAssignmentGradesAsync(int groupAssignmentId, IPrincipal principal, CancellationToken cancellationToken = default(CancellationToken), bool shouldSave = true);
	}
}