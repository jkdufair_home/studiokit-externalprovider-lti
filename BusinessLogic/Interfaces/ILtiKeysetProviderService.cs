﻿using System.Threading;
using Microsoft.IdentityModel.Tokens;
using StudioKit.ExternalProvider.Lti.Models;
using System.Threading.Tasks;

namespace StudioKit.ExternalProvider.Lti.BusinessLogic.Interfaces
{
	public interface ILtiKeySetProviderService
	{
		Task<JsonWebKeySet> GetKeySetAsync(LtiExternalProvider ltiExternalProvider, CancellationToken cancellationToken = default(CancellationToken));
	}
}