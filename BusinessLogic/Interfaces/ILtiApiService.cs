﻿using StudioKit.ExternalProvider.Lti.Models;
using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading;
using System.Threading.Tasks;

namespace StudioKit.ExternalProvider.Lti.BusinessLogic.Interfaces
{
	public interface ILtiApiService
	{
		Task<T> GetResultAsync<T>(LtiExternalProvider ltiExternalProvider, string url, CancellationToken cancellationToken = default(CancellationToken), MediaTypeWithQualityHeaderValue acceptHeaderValue = null);

		Task<T> PostAsync<T>(LtiExternalProvider ltiExternalProvider, string url,
			Func<Uri, HttpContent> getHttpContent,
			CancellationToken cancellationToken = default(CancellationToken),
			MediaTypeWithQualityHeaderValue acceptHeaderValue = null);
	}
}