﻿using StudioKit.ExternalProvider.Lti.Models;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Threading;
using System.Threading.Tasks;

namespace StudioKit.ExternalProvider.Lti.BusinessLogic.Interfaces
{
	public interface IJwtTokenService
	{
		Task<LtiExternalProvider> GetTokenProviderAsync(JwtSecurityToken token, CancellationToken cancellationToken = default(CancellationToken));

		Task AssertIsTokenValidAsync(string tokenString, CancellationToken cancellationToken = default(CancellationToken));

		Task<JwtSecurityToken> CreateJwtAsync(LtiExternalProvider externalProvider, List<Claim> claims, CancellationToken cancellationToken = default(CancellationToken));
	}
}