﻿using Newtonsoft.Json;
using StudioKit.ExternalProvider.Lti.BusinessLogic.Models;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;

namespace StudioKit.ExternalProvider.Lti.BusinessLogic.Utils
{
	public static class ClaimUtils
	{
		/// <summary>
		/// Given a collection of external Claims, generates new Claims where the Issuer is a combination of
		/// Issuer, ResourceLink Id, and Context Id.
		/// </summary>
		/// <param name="externalClaims">The claims to update</param>
		/// <returns>The new collection of claims</returns>
		public static List<Claim> UpdateExternalClaimsIssuer(ICollection<Claim> externalClaims)
		{
			var resourceLinkClaimValue = externalClaims.Single(c => c.Type.Equals(LtiClaim.ResourceLink)).Value;
			var resourceLink = JsonConvert.DeserializeObject<ResourceLink>(resourceLinkClaimValue);
			var contextClaimValue = externalClaims.Single(c => c.Type.Equals(LtiClaim.Context)).Value;
			var context = JsonConvert.DeserializeObject<Context>(contextClaimValue);

			return externalClaims
				.Select(ec => new Claim(ec.Type, ec.Value, ec.ValueType, $"{ec.Issuer}-{resourceLink.Id}-{context.Id}",
					ec.OriginalIssuer, ec.Subject))
				.ToList();
		}

		public static List<Claim> GetCommonClaims(IEnumerable<Claim> externalClaims)
		{
			return externalClaims
				.Where(c => !string.IsNullOrWhiteSpace(c.Value))
				.Join(LtiClaim.CommonClaimMap,
					c => c.Type,
					kvp => kvp.Key,
					(c, kvp) => new Claim(kvp.Value, c.Value, c.ValueType, c.Issuer, c.OriginalIssuer, c.Subject))
				.ToList();
		}

		public static List<string> GetExternalRoleNames(IEnumerable<Claim> externalClaims)
		{
			return externalClaims
				.Where(c => c.Type.Equals(LtiClaim.Roles))
				.Select(c => c.Value)
				.Distinct()
				.ToList();
		}

		public static List<string> GetGroupRoleNames(IEnumerable<Claim> externalClaims)
		{
			return GetGroupRoleNames(GetExternalRoleNames(externalClaims));
		}

		public static List<string> GetGroupRoleNames(List<string> externalRoleNames)
		{
			return externalRoleNames
				.Join(LtiRole.ContextRoleMap,
					name => name,
					kvp => kvp.Key,
					(name, kvp) => kvp.Value)
				.Distinct()
				.ToList();
		}

		public static List<string> GetApplicationRoleNames(IEnumerable<Claim> externalClaims)
		{
			return GetApplicationRoleNames(GetExternalRoleNames(externalClaims));
		}

		public static List<string> GetApplicationRoleNames(List<string> externalRoleNames)
		{
			return externalRoleNames
				.Join(LtiRole.ApplicationRoleMap,
					name => name,
					kvp => kvp.Key,
					(name, kvp) => kvp.Value)
				.Distinct()
				.ToList();
		}

		public static List<Claim> GetApplicationRoleClaims(IEnumerable<Claim> externalClaims)
		{
			return GetApplicationRoleNames(externalClaims)
				.Select(r => new Claim(ClaimTypes.Role, r))
				.ToList();
		}
	}
}