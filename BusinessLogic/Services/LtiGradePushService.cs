﻿using Newtonsoft.Json;
using StudioKit.ErrorHandling.Exceptions;
using StudioKit.ExternalProvider.DataAccess.Interfaces;
using StudioKit.ExternalProvider.Lti.BusinessLogic.Interfaces;
using StudioKit.ExternalProvider.Lti.BusinessLogic.Models;
using StudioKit.ExternalProvider.Lti.Models;
using StudioKit.ExternalProvider.Lti.Properties;
using StudioKit.ExternalProvider.Models;
using StudioKit.ExternalProvider.Models.Interfaces;
using StudioKit.Security;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Principal;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace StudioKit.ExternalProvider.Lti.BusinessLogic.Services
{
	public class LtiGradePushService<TGroup, TGroupUserRole, TExternalGroup, TExternalGroupUser, TExternalGroupAssignment, TGroupAssignment, TAssignment, TGroupAssignmentScore, TGroupAssignmentJobLog> : ILtiGradePushService
		where TGroup : class, IGroup<TGroupUserRole, TExternalGroup>
		where TGroupUserRole : class, IGroupUserRole
		where TExternalGroup : class, IExternalGroup
		where TExternalGroupUser : class, IExternalGroupUser<TExternalGroup>
		where TExternalGroupAssignment : class, IExternalGroupAssignment, new()
		where TGroupAssignment : class, IGroupAssignment
		where TAssignment : class, IAssignment
		where TGroupAssignmentScore : class, IGroupAssignmentScore
		where TGroupAssignmentJobLog : class, IGroupAssignmentJobLog, new()
	{
		private readonly IGradePushDbContext<TGroup, TGroupUserRole, TExternalGroup, TExternalGroupUser, TExternalGroupAssignment, TGroupAssignment,
			TAssignment, TGroupAssignmentScore, TGroupAssignmentJobLog> _dbContext;

		private readonly ILtiApiService _ltiApiService;

		public LtiGradePushService(IGradePushDbContext<TGroup, TGroupUserRole, TExternalGroup, TExternalGroupUser, TExternalGroupAssignment, TGroupAssignment, TAssignment, TGroupAssignmentScore, TGroupAssignmentJobLog> dbContext,
			ILtiApiService ltiApiService)
		{
			_dbContext = dbContext ?? throw new ArgumentNullException(nameof(dbContext));
			_ltiApiService = ltiApiService ?? throw new ArgumentNullException(nameof(ltiApiService));
		}

		public async Task PushGroupAssignmentGradesAsync(
			int groupAssignmentId,
			IPrincipal principal,
			CancellationToken cancellationToken = default(CancellationToken),
			bool shouldSave = true)
		{
			if (principal == null) throw new ArgumentNullException(nameof(principal));
			if (!(principal is SystemPrincipal))
				throw new ForbiddenException(string.Format(Security.Properties.Strings.RequiresSystemPrincipal, nameof(PushGroupAssignmentGradesAsync)));

			var groupAssignment = await _dbContext.GroupAssignments.SingleOrDefaultAsync(ga => ga.Id == groupAssignmentId && !ga.IsDeleted, cancellationToken);
			if (groupAssignment == null)
				throw new EntityNotFoundException(string.Format(Strings.EntityNotFoundById, typeof(TGroupAssignment).Name, groupAssignmentId));

			var assignment =
				await _dbContext.Assignments.SingleOrDefaultAsync(a => a.Id.Equals(groupAssignment.AssignmentId) && !a.IsDeleted,
					cancellationToken);
			if (assignment == null)
				throw new EntityNotFoundException(string.Format(Strings.EntityNotFoundById, typeof(TAssignment).Name, groupAssignment.AssignmentId));

			var group =
				await _dbContext.Groups.SingleOrDefaultAsync(g => g.Id.Equals(groupAssignment.GroupId) && !g.IsDeleted,
					cancellationToken);
			if (group == null)
				throw new EntityNotFoundException(string.Format(Strings.EntityNotFoundById, typeof(TGroup).Name, groupAssignment.GroupId));

			// ensure GroupAssignment grades have not already been successfully pushed before proceeding
			var logTypes = new List<string> { GradePushGroupAssignmentJobLogType.Pushed };
			var isAlreadyPushed = await _dbContext.GroupAssignmentJobLogs.AnyAsync(l =>
				l.GroupAssignmentId.Equals(groupAssignmentId) && logTypes.Contains(l.Type), cancellationToken);
			if (isAlreadyPushed)
				throw new ForbiddenException(Strings.GradePushAlreadyPushed);

			if (!assignment.Points.HasValue)
				throw new Exception(Strings.GradePushAssignmentPointsRequired);

			var externalGroupsQueryable = _dbContext.ExternalGroups
				.Where(eg =>
					eg.GroupId.Equals(groupAssignment.GroupId) &&
					eg.GradesUrl != null &&
					eg.GradesUrl.Trim() != string.Empty &&
					eg.ExternalProvider.GradePushEnabled);

			var externalGroups = await externalGroupsQueryable
				.Include(eg => eg.ExternalProvider)
				.ToListAsync(cancellationToken);
			if (!externalGroups.Any())
				throw new Exception(Strings.GradePushExternalGroupsRequired);

			var scores = await _dbContext.GetScoresAsync(groupAssignmentId, cancellationToken);
			if (scores == null || !scores.Any())
				throw new Exception(Strings.GradePushScoresRequired);

			var externalGroupUsers = await externalGroupsQueryable
				.Join(_dbContext.ExternalGroupUsers, eg => eg.Id, egu => egu.ExternalGroupId, (eg, egu) => egu)
				.ToListAsync(cancellationToken);

			var externalGroupAssignments = await _dbContext.ExternalGroupAssignments
				.Where(ega => ega.GroupAssignmentId.Equals(groupAssignmentId))
				.ToListAsync(cancellationToken);

			var externalGroupsWithNoExternalGroupAssignment = externalGroups
				.Where(eg => !externalGroupAssignments.Any(ega => ega.ExternalGroupId.Equals(eg.Id))).ToList();

			var externalGroupAssignmentsToAdd = new List<TExternalGroupAssignment>();
			foreach (var externalGroup in externalGroupsWithNoExternalGroupAssignment)
			{
				// Grade Push: Create new ExternalGroupAssignments
				var lineItemToCreate = new LineItem
				{
					ScoreMaximum = assignment.Points.Value,
					Label = assignment.Name,
					ResourceId = groupAssignment.Id.ToString()
				};
				// POST to externalGroup.GradesUrl
				var lineItem = await _ltiApiService.PostAsync<LineItem>(
					externalGroup.ExternalProvider as LtiExternalProvider,
					externalGroup.GradesUrl,
					uri => new StringContent(JsonConvert.SerializeObject(lineItemToCreate), Encoding.UTF8, LineItem.ContentType),
					cancellationToken,
					new MediaTypeWithQualityHeaderValue(LineItem.ContentType));
				// Create new ExternalGroupAssignments
				var externalGroupAssignment = new TExternalGroupAssignment
				{
					ExternalGroupId = externalGroup.Id,
					GroupAssignmentId = groupAssignmentId,
					ExternalId = lineItem.Id
				};
				// Add to list to be saved
				externalGroupAssignmentsToAdd.Add(externalGroupAssignment);
				// Add to list for future processing
				externalGroupAssignments.Add(externalGroupAssignment);
			}

			if (externalGroupAssignmentsToAdd.Any())
				_dbContext.ExternalGroupAssignments.AddRange(externalGroupAssignmentsToAdd);

			// INSERT ExternalGroupAssignments
			await _dbContext.SaveChangesAsync(cancellationToken);

			// send scores to endpoint
			foreach (var externalGroupAssignment in externalGroupAssignments)
			{
				var externalGroup = externalGroups.Single(eg => eg.Id.Equals(externalGroupAssignment.ExternalGroupId));
				foreach (var externalGroupUser in externalGroupUsers.Where(egu => egu.ExternalGroupId.Equals(externalGroupAssignment.ExternalGroupId)))
				{
					var score = scores.FirstOrDefault(s => s.CreatedById.Equals(externalGroupUser.UserId));
					if (score?.TotalPoints == null) continue;
					// Grade Push: POST score to GradesUrl/lineitems/id/scores
					var scoreToCreate = new Score
					{
						UserId = externalGroupUser.ExternalUserId,
						ActivityProgress = LtiAgsScoreActivityProgress.Completed,
						GradingProgress = LtiAgsScoreGradingProgress.FullyGraded,
						ScoreGiven = score.TotalPoints.Value,
						ScoreMaximum = assignment.Points.Value,
						Timestamp = score.DateLastUpdated
					};
					// POST result to externalGroup.GradesUrl/{id}/scores
					await _ltiApiService.PostAsync<Result>(
						externalGroup.ExternalProvider as LtiExternalProvider,
						$"{externalGroup.GradesUrl}/{externalGroupAssignment.ExternalId}/scores",
						uri => new StringContent(JsonConvert.SerializeObject(scoreToCreate), Encoding.UTF8, Score.ContentType),
						cancellationToken,
						new MediaTypeWithQualityHeaderValue(Result.ContentType));

					// TODO - Grade Push: handle specific errors in response
				}
			}

			if (!shouldSave) return;

			// INSERT GroupAssignmentJobLog
			await _dbContext.SaveChangesAsync(cancellationToken);
		}
	}
}