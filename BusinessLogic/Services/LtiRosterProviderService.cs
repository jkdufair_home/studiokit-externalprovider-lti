﻿using FluentValidation;
using StudioKit.Data.Entity.Identity.Interfaces;
using StudioKit.ExternalProvider.BusinessLogic.Interfaces;
using StudioKit.ExternalProvider.BusinessLogic.Models;
using StudioKit.ExternalProvider.Lti.BusinessLogic.Interfaces;
using StudioKit.ExternalProvider.Lti.BusinessLogic.Models;
using StudioKit.ExternalProvider.Lti.BusinessLogic.Utils;
using StudioKit.ExternalProvider.Lti.Models;
using StudioKit.ExternalProvider.Lti.Properties;
using StudioKit.ExternalProvider.Models.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Headers;
using System.Threading;
using System.Threading.Tasks;

namespace StudioKit.ExternalProvider.Lti.BusinessLogic.Services
{
	public class LtiRosterProviderService<TUser, TExternalGroup, TExternalGroupUser> : IRosterProviderService<TExternalGroup, TExternalGroupUser>
		where TUser : class, IUser, new()
		where TExternalGroup : class, IExternalGroup, new()
		where TExternalGroupUser : class, IExternalGroupUser<TExternalGroup>, new()
	{
		private readonly ILtiApiService _apiService;

		public LtiRosterProviderService(ILtiApiService apiService)
		{
			_apiService = apiService ?? throw new ArgumentNullException(nameof(apiService));
		}

		public async Task<IEnumerable<ExternalRosterEntry<TExternalGroup, TExternalGroupUser>>> GetRosterAsync(
			ExternalProvider.Models.ExternalProvider externalProvider,
			TExternalGroup externalGroup,
			CancellationToken cancellationToken = default(CancellationToken))
		{
			if (externalProvider == null) throw new ArgumentNullException(nameof(externalProvider));
			if (externalGroup == null) throw new ArgumentNullException(nameof(externalGroup));
			if (!(externalProvider is LtiExternalProvider ltiExternalProvider))
				throw new ArgumentException(Strings.LtiExternalProviderRequired);

			var roster = new List<ExternalRosterEntry<TExternalGroup, TExternalGroupUser>>();
			var membershipContainer = await _apiService.GetResultAsync<MembershipContainer>(ltiExternalProvider, externalGroup.RosterUrl, cancellationToken, new MediaTypeWithQualityHeaderValue(MembershipContainer.ContentType));

			// validate incoming membership data for required props
			if (membershipContainer.Members.Any(m => string.IsNullOrWhiteSpace(m.Email)))
				throw new ValidationException(Strings.MembersMissingEmail);

			foreach (var member in membershipContainer.Members)
			{
				// Skip adding member if they are not active, or already added
				if (!member.IsActive ||
					roster.Any(m => m.ExternalGroupUser.ExternalUserId.Equals(member.UserId) &&
									m.ExternalGroupUser.ExternalGroupId.Equals(externalGroup.Id))) continue;

				roster.Add(new ExternalRosterEntry<TExternalGroup, TExternalGroupUser>
				{
					ExternalGroupUser = new TExternalGroupUser
					{
						ExternalGroupId = externalGroup.Id,
						ExternalUserId = member.UserId,
						User = new TUser
						{
							UserName = member.Email,
							Email = member.Email,
							FirstName = member.GivenName,
							LastName = member.FamilyName,
							EmployeeNumber = member.UserId,
							Uid = member.Email?.Substring(0, member.Email.IndexOf("@", StringComparison.Ordinal))
						}
					},
					GroupUserRoles = ClaimUtils.GetGroupRoleNames(member.Roles)
				});
			}

			// filter roster by supported roles
			roster = roster.Where(r => r.GroupUserRoles.Any()).ToList();

			return roster;
		}
	}
}