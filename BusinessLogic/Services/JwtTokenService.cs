﻿using Microsoft.IdentityModel.Tokens;
using Org.BouncyCastle.Crypto;
using Org.BouncyCastle.Crypto.Parameters;
using Org.BouncyCastle.OpenSsl;
using StudioKit.Encryption;
using StudioKit.ErrorHandling.Exceptions;
using StudioKit.ExternalProvider.Lti.BusinessLogic.Interfaces;
using StudioKit.ExternalProvider.Lti.BusinessLogic.Models;
using StudioKit.ExternalProvider.Lti.DataAccess;
using StudioKit.ExternalProvider.Lti.Exceptions;
using StudioKit.ExternalProvider.Lti.Models;
using StudioKit.ExternalProvider.Lti.Properties;
using StudioKit.ExternalProvider.Models.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IdentityModel.Tokens.Jwt;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Threading;
using System.Threading.Tasks;

namespace StudioKit.ExternalProvider.Lti.BusinessLogic.Services
{
	public class JwtTokenService<TGroupUserRole, TGroupUserRoleLog, TExternalGroup, TExternalGroupUser> : IJwtTokenService
		where TGroupUserRole : class, IGroupUserRole, new()
		where TGroupUserRoleLog : class, IGroupUserRoleLog, new()
		where TExternalGroup : class, IExternalGroup, new()
		where TExternalGroupUser : class, IExternalGroupUser<TExternalGroup>, new()
	{
		private readonly ILtiLaunchDbContext<TGroupUserRole, TGroupUserRoleLog, TExternalGroup, TExternalGroupUser> _dbContext;
		private readonly ILtiKeySetProviderService _keySetProviderService;
		private readonly ILtiConfigurationProvider _configurationProvider;

		public JwtTokenService(ILtiLaunchDbContext<TGroupUserRole, TGroupUserRoleLog, TExternalGroup, TExternalGroupUser> dbContext,
			ILtiKeySetProviderService keySetProviderService, ILtiConfigurationProvider configurationProvider)
		{
			_dbContext = dbContext;
			_keySetProviderService = keySetProviderService;
			_configurationProvider = configurationProvider;
		}

		public async Task<LtiExternalProvider> GetTokenProviderAsync(JwtSecurityToken token, CancellationToken cancellationToken = default(CancellationToken))
		{
			if (token == null) throw new ArgumentNullException(nameof(token));

			// Get the oauth2 client_id from the token
			var audiences = token.Audiences.ToList();

			var deploymentId = token.Claims.SingleOrDefault(c => c.Type.Equals(LtiClaim.DeploymentId))?.Value;
			if (deploymentId == null)
				return null;

			// Check that the audiences claim contains our client ID
			// If we do start trusting multiple clients
			return await _dbContext.LtiExternalProviders
				.SingleOrDefaultAsync(p => audiences.Contains(p.OauthClientId) && p.DeploymentId == deploymentId, cancellationToken);
		}

		/// <summary>
		/// Checks the payload of a platform originating JWT for required claims and
		/// verifies the signature
		/// </summary>
		/// <param name="tokenString">The JWT as a string</param>
		/// <param name="cancellationToken">A <see cref="CancellationToken"/> from the caller</param>
		/// <exception cref="UnauthorizedException"></exception>
		public async Task AssertIsTokenValidAsync(string tokenString, CancellationToken cancellationToken = default(CancellationToken))
		{
			if (string.IsNullOrWhiteSpace(tokenString)) throw new ArgumentNullException(nameof(tokenString));

			// Decode string (without validating) to be able to get the provider.
			var token = new JwtSecurityToken(tokenString);

			if (token.Issuer == null)
				throw new UnauthorizedException(string.Format(Strings.TokenMissingRequiredClaim, LtiClaim.Issuer));

			if (!token.Audiences.Any())
				throw new UnauthorizedException(string.Format(Strings.TokenMissingRequiredClaim, LtiClaim.Audience));

			if (token.Subject == null)
				throw new UnauthorizedException(string.Format(Strings.TokenMissingRequiredClaim, LtiClaim.Subject));

			if (token.ValidTo == DateTime.MinValue)
				throw new UnauthorizedException(string.Format(Strings.TokenMissingRequiredClaim, LtiClaim.Expiration));

			if (!token.Claims.Any(c => c.Type.Equals(LtiClaim.IssuedAt) && c.Value != null))
				throw new UnauthorizedException(string.Format(Strings.TokenMissingRequiredClaim, LtiClaim.IssuedAt));

			if (!token.Claims.Any(c => c.Type.Equals(LtiClaim.Nonce) && c.Value != null))
				throw new UnauthorizedException(string.Format(Strings.TokenMissingRequiredClaim, LtiClaim.Nonce));

			// Get the oauth2 client_id from the token
			var audiences = token.Audiences.ToList();

			// If the audiences claim has more than one item, reject it if we don't trust all audiences
			// (and we don't trust anyone else right now)
			// If we do trust people in the future, the 'azp' claim must be present if more than one audience exists
			if (audiences.Count > 1)
				throw new UnauthorizedException(Strings.TokenUntrustedAudiences);

			var deploymentId = token.Claims.SingleOrDefault(c => c.Type.Equals(LtiClaim.DeploymentId))?.Value;
			if (deploymentId == null)
				throw new UnauthorizedException(string.Format(Strings.TokenMissingRequiredClaim, LtiClaim.DeploymentId));

			// Check that the audiences claim contains our client ID
			// If we do start trusting multiple clients
			var externalProvider = await GetTokenProviderAsync(token, cancellationToken);
			if (externalProvider == null)
				throw new UnauthorizedException(Strings.AudienceDeploymentIdNotRegistered);

			// If the 'authorized party' claim exists check that it equals the registered client_id value.
			if (token.Claims.Any(c => c.Type.Equals(LtiClaim.AuthorizedParty) && !c.Value.Equals(externalProvider.OauthClientId)))
				throw new UnauthorizedException(Strings.AzpClaimDoesNotMatchClientId);

			// Get public keys from the platform
			var jsonWebKeySet = await _keySetProviderService.GetKeySetAsync(externalProvider, cancellationToken);

			var tokenHandler = new JwtSecurityTokenHandler();
			// Set required values for some claims based on the provider.
			var validationParams = new TokenValidationParameters
			{
				ValidateAudience = true,
				ValidateIssuer = true,
				ValidateLifetime = true,
				ValidateIssuerSigningKey = true,
				ValidAudience = externalProvider.OauthClientId,
				ValidIssuer = externalProvider.Issuer,
				IssuerSigningKeys = jsonWebKeySet.GetSigningKeys()
			};

			// Verify token signature
			try
			{
				tokenHandler.ValidateToken(tokenString, validationParams, out var _);
			}
			catch (SecurityTokenException e)
			{
				throw new UnauthorizedException(Strings.ValidateTokenFailed, e);
			}
		}

		/// <summary>
		/// Create a Json Web Token with the required LTI claims with values from the given LtiExternalProvider
		/// and including the given claims
		/// </summary>
		/// <param name="provider"><see cref="LtiExternalProvider"/> where this token will be sent</param>
		/// <param name="claimList">Claims that should be included in the created JWT</param>
		/// <param name="cancellationToken">A <see cref="CancellationToken"/> from the caller</param>
		/// <returns>A <see cref="JwtSecurityToken"/> with the required LTI claims set</returns>
		public async Task<JwtSecurityToken> CreateJwtAsync(LtiExternalProvider provider, List<Claim> claimList = null, CancellationToken cancellationToken = default(CancellationToken))
		{
			if (provider == null)
				throw new ArgumentNullException(nameof(provider));

			if (claimList == null)
				claimList = new List<Claim>();

			var ltiConfig = await _configurationProvider.GetLtiConfigurationAsync(cancellationToken);
			var privateKeyEncryptedString = System.Text.Encoding.UTF8.GetString(ltiConfig.PrivateRsaKey);
			var privateKeyString = EncryptedConfigurationManager.TryDecryptSettingValue(privateKeyEncryptedString);

			var currentTime = DateTime.UtcNow;
			string audience = null;
			DateTime? expires = null;

			// Issuer is always going to be our tool ID for the target platform
			if (claimList.Any(c => c.Type.Equals(LtiClaim.Issuer) && !c.Value.Equals(provider.OauthClientId)))
			{
				var invalidClaim = claimList.First(c => c.Type.Equals(LtiClaim.Issuer));
				throw new LtiInvalidJwtException(string.Format(Strings.LtiInvalidJwtClaim, invalidClaim.Type, invalidClaim.Value));
			}

			// Check if the required claims already exist in the passed in claim List.
			// Set default values to use in the constructor if they do not exist.
			if (!claimList.Any(c => c.Type.Equals(LtiClaim.Audience)))
			{
				audience = provider.Issuer;
			}

			if (!claimList.Any(c => c.Type.Equals(LtiClaim.Expiration)))
			{
				expires = currentTime.AddMinutes(3);
			}

			if (!claimList.Any(c => c.Type.Equals(LtiClaim.IssuedAt)))
			{
				var epochTimeSpan = currentTime.Subtract(new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc));
				claimList.Add(new Claim(LtiClaim.IssuedAt, Convert.ToInt64(epochTimeSpan.TotalSeconds).ToString(), ClaimValueTypes.Integer64));
			}

			if (!claimList.Any(c => c.Type.Equals(LtiClaim.Nonce)))
			{
				claimList.Add(new Claim(LtiClaim.Nonce, Guid.NewGuid().ToString()));
			}

			using (var publicKeyReader = new StringReader(privateKeyString))
			{
				var keyPair = (AsymmetricCipherKeyPair)new PemReader(publicKeyReader).ReadObject();
				var privateKeyParams = (RsaPrivateCrtKeyParameters)keyPair.Private;

				var rsaParams = new RSAParameters
				{
					Exponent = privateKeyParams.PublicExponent.ToByteArrayUnsigned(),
					Modulus = privateKeyParams.Modulus.ToByteArrayUnsigned(),
					D = privateKeyParams.Exponent.ToByteArrayUnsigned(),
					DQ = privateKeyParams.DQ.ToByteArrayUnsigned(),
					DP = privateKeyParams.DP.ToByteArrayUnsigned(),
					P = privateKeyParams.P.ToByteArrayUnsigned(),
					Q = privateKeyParams.Q.ToByteArrayUnsigned(),
					InverseQ = privateKeyParams.QInv.ToByteArrayUnsigned()
				};
				var credentials = new SigningCredentials(new RsaSecurityKey(rsaParams), SecurityAlgorithms.RsaSha256);

				// The constructor here adds an 'nbf' (not before) claim instead of 'iat' (issued at). We don't need it.
				return new JwtSecurityToken(provider.OauthClientId, audience, claimList, null, expires, credentials);
			}
		}
	}
}