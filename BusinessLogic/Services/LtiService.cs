﻿using FluentValidation;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Newtonsoft.Json;
using StudioKit.Data.Entity.Identity;
using StudioKit.ErrorHandling.Exceptions;
using StudioKit.ExternalProvider.Lti.BusinessLogic.Interfaces;
using StudioKit.ExternalProvider.Lti.BusinessLogic.Models;
using StudioKit.ExternalProvider.Lti.BusinessLogic.Utils;
using StudioKit.ExternalProvider.Lti.DataAccess;
using StudioKit.ExternalProvider.Lti.Models;
using StudioKit.ExternalProvider.Lti.Properties;
using StudioKit.ExternalProvider.Models.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Threading;
using System.Threading.Tasks;

namespace StudioKit.ExternalProvider.Lti.BusinessLogic.Services
{
	public class LtiService<TGroupUserRole, TGroupUserRoleLog, TExternalGroup, TExternalGroupUser> : ILtiService
		where TGroupUserRole : class, IGroupUserRole, new()
		where TGroupUserRoleLog : class, IGroupUserRoleLog, new()
		where TExternalGroup : class, IExternalGroup, new()
		where TExternalGroupUser : class, IExternalGroupUser<TExternalGroup>, new()
	{
		private readonly ILtiLaunchDbContext<TGroupUserRole, TGroupUserRoleLog, TExternalGroup, TExternalGroupUser> _dbContext;
		private readonly IJwtTokenService _jwtTokenService;

		public LtiService(ILtiLaunchDbContext<TGroupUserRole, TGroupUserRoleLog, TExternalGroup, TExternalGroupUser> dbContext,
			IJwtTokenService jwtTokenService)
		{
			_dbContext = dbContext;
			_jwtTokenService = jwtTokenService;
		}

		public string GetLaunchReturnUrl(string tokenString)
		{
			if (string.IsNullOrWhiteSpace(tokenString)) throw new ArgumentNullException(nameof(tokenString));
			var token = new JwtSecurityToken(tokenString);
			var claims = token.Claims.ToList();
			var launchPresentationClaim = claims.SingleOrDefault(c => c.Type == LtiClaim.LaunchPresentation);
			if (launchPresentationClaim == null)
				return null;
			var launchPresentation = JsonConvert.DeserializeObject<LaunchPresentation>(launchPresentationClaim.Value);
			return launchPresentation.ReturnUrl;
		}

		public async Task<ExternalLoginInfo> GetLoginInfoAsync(string tokenString, CancellationToken cancellationToken = default(CancellationToken))
		{
			if (string.IsNullOrWhiteSpace(tokenString)) throw new ArgumentNullException(nameof(tokenString));

			await _jwtTokenService.AssertIsTokenValidAsync(tokenString, cancellationToken);

			var token = new JwtSecurityToken(tokenString);
			var claims = token.Claims.ToList();
			AssertLaunchClaimsAreValid(claims);
			var externalProvider = await _jwtTokenService.GetTokenProviderAsync(token, cancellationToken);
			var updatedClaims = ClaimUtils.UpdateExternalClaimsIssuer(claims);
			var externalUserId = updatedClaims.Single(x => x.Type == LtiClaim.Subject).Value;
			var email = updatedClaims.Single(x => x.Type == LtiClaim.Email).Value;
			var commonClaims = ClaimUtils.GetCommonClaims(updatedClaims);
			var applicationRoleClaims = ClaimUtils.GetApplicationRoleClaims(updatedClaims);

			return new ExternalLoginInfo
			{
				DefaultUserName = email,
				Email = email,
				ExternalIdentity = new ClaimsIdentity(updatedClaims.Concat(commonClaims).Concat(applicationRoleClaims)),
				Login = new UserLoginInfo(string.Format(Strings.LoginProviderFormat, externalProvider.Id), externalUserId)
			};
		}

		public async Task<LaunchResponse> GetLaunchResponseAsync(string tokenString, string userId, List<Claim> claims, CancellationToken cancellationToken = default(CancellationToken))
		{
			if (string.IsNullOrWhiteSpace(tokenString)) throw new ArgumentNullException(nameof(tokenString));
			if (string.IsNullOrWhiteSpace(userId)) throw new ArgumentNullException(nameof(userId));
			await _jwtTokenService.AssertIsTokenValidAsync(tokenString, cancellationToken);
			AssertLaunchClaimsAreValid(claims);

			var token = new JwtSecurityToken(tokenString);
			var externalProvider = await _jwtTokenService.GetTokenProviderAsync(token, cancellationToken);
			var externalUserId = claims.Single(x => x.Type == LtiClaim.Subject).Value;

			// Get list of Context roles from claims
			var externalRoleNames = ClaimUtils.GetExternalRoleNames(claims);
			var externalRolesString = string.Join(",", externalRoleNames);
			var groupRoleNames = ClaimUtils.GetGroupRoleNames(claims);
			var isGroupOwner = groupRoleNames.Any(r => r.Equals(BaseRole.GroupOwner));

			// Get LTI context data
			var contextClaimValue = claims.Single(c => c.Type.Equals(LtiClaim.Context)).Value;
			var context = JsonConvert.DeserializeObject<Context>(contextClaimValue);

			// Get Names and Role Provisioning Service
			var namesRoleServiceClaimValue = claims.SingleOrDefault(c => c.Type.Equals(LtiNrpsClaim.NamesRoleService));
			var namesRoleService = namesRoleServiceClaimValue != null
				? JsonConvert.DeserializeObject<NamesRoleService>(namesRoleServiceClaimValue.Value)
				: null;

			// Get Endpoint claim
			var endpointClaimValue = claims.SingleOrDefault(c => c.Type.Equals(LtiAgsClaim.Endpoint));
			var endpoint = endpointClaimValue != null
				? JsonConvert.DeserializeObject<Endpoint>(endpointClaimValue.Value)
				: null;

			var externalGroup = await _dbContext.ExternalGroups
				.SingleOrDefaultAsync(g =>
					g.ExternalId.Equals(context.Id) &&
					g.ExternalProviderId.Equals(externalProvider.Id), cancellationToken);

			// If a student uses a created link to launch the tool
			// but the instructor never completed the group creation, don't do anything
			if (!isGroupOwner && externalGroup == null)
				throw new ForbiddenException(Strings.ContentNotAvailable);

			// Launch Context has been setup in the past
			if (externalGroup != null)
			{
				// update ExternalGroup details
				externalGroup.Name = context.Title ?? context.Id;
				externalGroup.Description = context.Label;
				externalGroup.RosterUrl = namesRoleService?.ContextMembershipsUrl;
				externalGroup.GradesUrl = endpoint?.LineItems;

				// add or update ExternalGroupUser
				var externalGroupUser = await _dbContext.ExternalGroupUsers
					.SingleOrDefaultAsync(egu =>
						egu.ExternalGroupId.Equals(externalGroup.Id) &&
						egu.ExternalUserId.Equals(externalUserId) &&
						egu.UserId.Equals(userId), cancellationToken);
				if (externalGroupUser == null)
				{
					externalGroupUser = new TExternalGroupUser
					{
						UserId = userId,
						Roles = externalRolesString,
						ExternalUserId = externalUserId,
						ExternalGroupId = externalGroup.Id
					};
					_dbContext.ExternalGroupUsers.Add(externalGroupUser);
				}
				else
				{
					externalGroupUser.Roles = externalRolesString;
				}

				// add GroupUserRoles (if needed)
				var roles = await _dbContext.Roles.ToListAsync(cancellationToken);
				var existingGroupUserRoles = (await _dbContext.GroupUserRoles
					.Where(gur =>
						gur.GroupId.Equals(externalGroup.GroupId) &&
						gur.UserId.Equals(userId))
					.ToListAsync(cancellationToken))
					.Select(gur => roles.Single(r => r.Id.Equals(gur.RoleId)).Name)
					.ToList();
				var groupUserRolesToAdd = groupRoleNames
					.Where(r => !existingGroupUserRoles.Contains(r))
					.Select(n => new TGroupUserRole
					{
						GroupId = externalGroup.GroupId,
						UserId = userId,
						RoleId = roles.Single(r => r.Name.Equals(n)).Id,
						IsExternal = true
					})
					.ToList();
				if (groupUserRolesToAdd.Any())
				{
					_dbContext.GroupUserRoles.AddRange(groupUserRolesToAdd);
					_dbContext.GroupUserRoleLogs.AddRange(groupUserRolesToAdd
						.Select(gur => new TGroupUserRoleLog
						{
							GroupId = gur.GroupId,
							UserId = gur.UserId,
							RoleId = gur.RoleId,
							Type = GroupUserRoleLogType.Added
						})
						.ToList());
				}

				await _dbContext.SaveChangesAsync(cancellationToken);
				return new LaunchResponse { GroupId = externalGroup.GroupId };
			}

			// Save external launch info
			var versionClaim = claims.Single(c => c.Type.Equals(LtiClaim.Version)).Value;
			var presentationClaimValue = claims.Single(c => c.Type.Equals(LtiClaim.LaunchPresentation)).Value;
			var presentation = JsonConvert.DeserializeObject<LaunchPresentation>(presentationClaimValue);
			var launchDetails = new LtiLaunch
			{
				CreatedById = userId,
				ExternalProviderId = externalProvider.Id,
				ExternalId = context.Id,
				Name = context.Title ?? context.Id,
				Description = context.Label,
				Roles = externalRolesString,
				LtiVersion = versionClaim,
				ReturnUrl = presentation.ReturnUrl,
				RosterUrl = namesRoleService?.ContextMembershipsUrl,
				GradesUrl = endpoint?.LineItems
			};
			_dbContext.LtiLaunches.Add(launchDetails);
			await _dbContext.SaveChangesAsync(cancellationToken);

			return new LaunchResponse { LtiLaunchId = launchDetails.Id };
		}

		public void AssertLaunchClaimsAreValid(List<Claim> claims)
		{
			// Check for required claims and values for launch. User claim is also required, but checking for a valid token above checks that claim exists.
			if (!claims.Any(c => c.Type.Equals(LtiClaim.MessageType) && c.Value.Equals(LtiMessageType.LtiResourceLinkRequest)))
				throw new ValidationException(string.Format(Strings.TokenMissingRequiredClaimAndValue, LtiClaim.MessageType, LtiMessageType.LtiResourceLinkRequest));

			// In case the spec document doesn't get updated, '1.3.0' is the new correct value
			if (!claims.Any(c => c.Type.Equals(LtiClaim.Version) && c.Value.Equals(LtiVersion.Lti1P3)))
				throw new ValidationException(string.Format(Strings.TokenMissingRequiredClaimAndValue, LtiClaim.Version, LtiVersion.Lti1P3));

			// Technically a required attribute for a launch even though we don't use it
			var resourceLinkClaim = claims.SingleOrDefault(c => c.Type.Equals(LtiClaim.ResourceLink));
			if (resourceLinkClaim == null)
				throw new ValidationException(string.Format(Strings.TokenMissingRequiredClaim, LtiClaim.ResourceLink));

			var resourceLink = JsonConvert.DeserializeObject<ResourceLink>(resourceLinkClaim.Value);
			if (string.IsNullOrWhiteSpace(resourceLink.Id))
				throw new ValidationException(string.Format(Strings.TokenMissingRequiredClaim, $"{LtiClaim.ResourceLink}.id"));

			var contextClaim = claims.SingleOrDefault(c => c.Type.Equals(LtiClaim.Context));
			if (contextClaim == null)
				throw new ValidationException(string.Format(Strings.TokenMissingRequiredClaim, LtiClaim.Context));

			var context = JsonConvert.DeserializeObject<Context>(contextClaim.Value);
			if (string.IsNullOrWhiteSpace(context.Id))
				throw new ValidationException(string.Format(Strings.TokenMissingRequiredClaim, $"{LtiClaim.Context}.id"));

			var emailClaim = claims.FirstOrDefault(x => x.Type == LtiClaim.Email);
			if (emailClaim?.Value == null)
				throw new ValidationException(string.Format(Strings.TokenMissingRequiredClaim, LtiClaim.Email));
		}
	}
}