﻿using Newtonsoft.Json;
using StudioKit.ErrorHandling.Interfaces;
using StudioKit.ExternalProvider.Lti.BusinessLogic.Interfaces;
using StudioKit.ExternalProvider.Lti.BusinessLogic.Models;
using StudioKit.ExternalProvider.Lti.Exceptions;
using StudioKit.ExternalProvider.Lti.Models;
using StudioKit.ExternalProvider.Lti.Properties;
using StudioKit.TransientFaultHandling.Http.Constants;
using StudioKit.TransientFaultHandling.Http.Interfaces;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Claims;
using System.Threading;
using System.Threading.Tasks;
using JsonException = Newtonsoft.Json.JsonException;

namespace StudioKit.ExternalProvider.Lti.BusinessLogic.Services
{
	public class LtiApiService : ILtiApiService
	{
		private static readonly ConcurrentDictionary<int, OauthToken> OauthTokenCache;
		private readonly IJwtTokenService _tokenService;
		private readonly Func<IHttpClient> _getHttpClient;
		private readonly IErrorHandler _errorHandler;

		static LtiApiService()
		{
			OauthTokenCache = new ConcurrentDictionary<int, OauthToken>();
		}

		public static void ClearCache()
		{
			OauthTokenCache.Clear();
		}

		public LtiApiService(IJwtTokenService tokenService, Func<IHttpClient> getHttpClient, IErrorHandler errorHandler)
		{
			_tokenService = tokenService;
			_getHttpClient = getHttpClient;
			_errorHandler = errorHandler;
		}

		public async Task<T> GetResultAsync<T>(LtiExternalProvider ltiExternalProvider, string url, CancellationToken cancellationToken = default(CancellationToken), MediaTypeWithQualityHeaderValue acceptHeaderValue = null)
		{
			if (ltiExternalProvider == null)
				throw new ArgumentNullException(nameof(ltiExternalProvider));

			if (url == null)
				throw new ArgumentNullException(nameof(url));

			var client = _getHttpClient();
			var token = await GetAuthTokenAsync(ltiExternalProvider, cancellationToken);
			var authorizationHeader = new AuthenticationHeaderValue(AuthorizationHeaderType.Bearer, token);

			try
			{
				var responseBody = await client
					.GetStringAsync(new Uri(url), cancellationToken, authorizationHeader, acceptHeaderValue)
					.ConfigureAwait(false);

				return JsonConvert.DeserializeObject<T>(responseBody);
			}
			catch (HttpRequestException e)
			{
				throw new LtiException(Strings.LtiApiRequestError, e);
			}
			catch (JsonException e)
			{
				throw new LtiException(Strings.LtiJsonError, e);
			}
		}

		public async Task<T> PostAsync<T>(LtiExternalProvider ltiExternalProvider, string url, Func<Uri, HttpContent> getHttpContent,
			CancellationToken cancellationToken = default(CancellationToken),
			MediaTypeWithQualityHeaderValue acceptHeaderValue = null)
		{
			if (ltiExternalProvider == null)
				throw new ArgumentNullException(nameof(ltiExternalProvider));

			if (url == null)
				throw new ArgumentNullException(nameof(url));

			if (getHttpContent == null)
				throw new ArgumentNullException(nameof(getHttpContent));

			var client = _getHttpClient();
			var token = await GetAuthTokenAsync(ltiExternalProvider, cancellationToken);
			var authorizationHeader = new AuthenticationHeaderValue(AuthorizationHeaderType.Bearer, token);

			try
			{
				var responseBody = await client.PostAsync(new Uri(url), getHttpContent, cancellationToken, authorizationHeader, acceptHeaderValue)
					.ConfigureAwait(false);

				return JsonConvert.DeserializeObject<T>(responseBody);
			}
			catch (HttpRequestException e)
			{
				throw new LtiException(Strings.LtiApiRequestError, e);
			}
			catch (JsonException e)
			{
				throw new LtiException(Strings.LtiJsonError, e);
			}
		}

		public async Task<string> GetAuthTokenAsync(LtiExternalProvider provider, CancellationToken cancellationToken = default(CancellationToken))
		{
			if (provider == null)
				throw new ArgumentNullException(nameof(provider));

			OauthTokenCache.TryGetValue(provider.Id, out var providerToken);

			//If the token has less than 5 minutes left, go ahead and get a new one.
			if (providerToken != null && DateTime.UtcNow.AddMinutes(5) < providerToken.ExpirationTime)
				return providerToken.AccessToken;

			var client = _getHttpClient();

			var claims = new List<Claim>
			{
				new Claim(LtiClaim.Subject, provider.OauthClientId),
				new Claim(LtiClaim.Audience, provider.OauthTokenUrl),
				new Claim(LtiClaim.JwtId, Guid.NewGuid().ToString())
			};
			var authRequestToken = await _tokenService.CreateJwtAsync(provider, claims, cancellationToken);
			var handler = new JwtSecurityTokenHandler();
			var tokenString = handler.WriteToken(authRequestToken);
			var requestStartTime = DateTime.UtcNow;
			OauthToken responseToken;

			try
			{
				var json = await client.PostAsync(
						new Uri(provider.OauthTokenUrl),
						uri => new FormUrlEncodedContent(new[]
						{
							new KeyValuePair<string, string>(OAuthConstants.ClientAssertion, tokenString),
							new KeyValuePair<string, string>(OAuthConstants.ClientAssertionType,
								OAuthConstants.JwtBearerAssertionType),
							new KeyValuePair<string, string>(OAuthConstants.GrantType,
								OAuthConstants.ClientCredentialsGrant),
							new KeyValuePair<string, string>(OAuthConstants.ScopeKey,
								$"{LtiNrpsScope.ContextMembershipsReadOnly} {LtiAgsScope.LineItem} {LtiAgsScope.ResultReadOnly} {LtiAgsScope.Score}")
						}),
						cancellationToken)
					.ConfigureAwait(false);

				responseToken = JsonConvert.DeserializeObject<OauthToken>(json);
			}
			catch (HttpRequestException e)
			{
				//If the original token hasn't expired, just use that
				if (providerToken != null && DateTime.UtcNow < providerToken.ExpirationTime)
				{
					_errorHandler.CaptureException(e);
					return providerToken.AccessToken;
				}

				throw new LtiException(Strings.LtiOauthRequestError, e);
			}
			catch (JsonException e)
			{
				throw new LtiException(Strings.LtiJsonError, e);
			}

			responseToken.ExpirationTime = requestStartTime.AddSeconds(responseToken.ValidDuration);

			OauthTokenCache[provider.Id] = responseToken;

			return responseToken.AccessToken;
		}
	}
}