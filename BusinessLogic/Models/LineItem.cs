﻿namespace StudioKit.ExternalProvider.Lti.BusinessLogic.Models
{
	public class LineItem
	{
		public static string ContentType = "application/vnd.ims.lis.v2.lineitem+json";

		public string Id { get; set; }

		public decimal ScoreMaximum { get; set; }

		public string Label { get; set; }

		public string ResourceId { get; set; }

		public string Tag { get; set; }

		public string LtiLinkId { get; set; }
	}
}