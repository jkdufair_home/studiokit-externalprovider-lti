﻿using System.Collections.Generic;

namespace StudioKit.ExternalProvider.Lti.BusinessLogic.Models
{
	public class MembershipContainer
	{
		public static string ContentType = "application/vnd.ims.lis.v2.membershipcontainer+json";

		public string Id { get; set; }

		public List<Member> Members { get; set; }
	}
}