﻿namespace StudioKit.ExternalProvider.Lti.BusinessLogic.Models
{
	public class LaunchResponse
	{
		public int? LtiLaunchId { get; set; }

		public int? GroupId { get; set; }
	}
}