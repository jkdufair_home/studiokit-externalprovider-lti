﻿using System.Collections.Generic;
using System.Security.Claims;

namespace StudioKit.ExternalProvider.Lti.BusinessLogic.Models
{
	public static class LtiClaim
	{
		public static string Context = "https://purl.imsglobal.org/spec/lti/claim/context";

		public static string MessageType = "https://purl.imsglobal.org/spec/lti/claim/message_type";

		public static string Version = "https://purl.imsglobal.org/spec/lti/claim/version";

		public static string DeploymentId = "https://purl.imsglobal.org/spec/lti/claim/deployment_id";

		public static string ResourceLink = "https://purl.imsglobal.org/spec/lti/claim/resource_link";

		public static string Platform = "https://purl.imsglobal.org/spec/lti/claim/tool_platform";

		public static string Roles = "https://purl.imsglobal.org/spec/lti/claim/roles";

		public static string RoleScopeMentor = "https://purlimsglobal.org/spec/lti/claim/role_scope_mentor";

		public static string LaunchPresentation = "https://purl.imsglobal.org/spec/lti/claim/launch_presentation";

		public static string Custom = "https://purl.imsglobal.org/spec/lti/claim/custom";

		public static string Lis = "https://purl.imsglobal.org/spec/lti/claim/lis";

		/// <summary>
		/// Identifier for the object this token refers to
		/// </summary>
		public static string Subject = "sub";

		/// <summary>
		/// Identifier for the creator of the token
		/// </summary>
		public static string Issuer = "iss";

		/// <summary>
		/// Identifier for a party intended to receive this token, may have more than one
		/// </summary>
		public static string Audience = "aud";

		/// <summary>
		/// Time the token was issued as seconds since the Unix epoch
		/// </summary>
		public static string IssuedAt = "iat";

		/// <summary>
		/// Time the token will expire as seconds since the Unix epoch
		/// </summary>
		public static string Expiration = "exp";

		/// <summary>
		/// Identifier for the party to which this token was issued
		/// </summary>
		public static string AuthorizedParty = "azp";

		/// <summary>
		/// Unique ID for this token
		/// </summary>
		public static string JwtId = "jti";

		public static string Nonce = "nonce";

		public static string Email = "email";

		public static string GivenName = "given_name";

		public static string FamilyName = "family_name";

		public static string Name = "name";

		public static string MiddleName = "middle_name";

		public static string Picture = "picture";

		public static string Locale = "locale";

		public static Dictionary<string, string> CommonClaimMap => new Dictionary<string, string>
		{
			{Subject, ClaimTypes.NameIdentifier},
			{Email, ClaimTypes.Email},
			{GivenName, ClaimTypes.GivenName},
			{FamilyName, ClaimTypes.Surname}
		};
	}
}