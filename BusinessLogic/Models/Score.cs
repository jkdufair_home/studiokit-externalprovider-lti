﻿using System;

namespace StudioKit.ExternalProvider.Lti.BusinessLogic.Models
{
	public class Score
	{
		public static string ContentType = "application/vnd.ims.lis.v1.score+json";

		public string UserId { get; set; }

		public decimal ScoreGiven { get; set; }

		public decimal ScoreMaximum { get; set; }

		public string Comment { get; set; }

		public DateTime Timestamp { get; set; }

		public string ActivityProgress { get; set; }

		public string GradingProgress { get; set; }
	}
}