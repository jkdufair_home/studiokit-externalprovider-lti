﻿namespace StudioKit.ExternalProvider.Lti.BusinessLogic.Models
{
	public class LtiAgsScoreGradingProgress
	{
		public static string FullyGraded = "FullyGraded";

		public static string Pending = "Pending";

		public static string PendingManual = "PendingManual";

		public static string Failed = "Failed";

		public static string NotReady = "NotReady";
	}
}