﻿namespace StudioKit.ExternalProvider.Lti.BusinessLogic.Models
{
	public class Result
	{
		public static string ContentType = "application/vnd.ims.lis.v2.result+json";

		public string Id { get; set; }

		public string ScoreOf { get; set; }

		public string UserId { get; set; }

		public decimal ResultScore { get; set; }

		public decimal ResultMaximum { get; set; }

		public string Comment { get; set; }
	}
}