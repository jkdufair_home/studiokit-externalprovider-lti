﻿namespace StudioKit.ExternalProvider.Lti.BusinessLogic.Models
{
	public class OAuthConstants
	{
		public static string ClientAssertion = "client_assertion";

		public static string ClientAssertionType = "client_assertion_type";

		public static string JwtBearerAssertionType = "urn:ietf:params:oauth:client-assertion-type:jwt-bearer";

		public static string GrantType = "grant_type";

		public static string ClientCredentialsGrant = "client_credentials";

		public static string ScopeKey = "scope";
	}
}