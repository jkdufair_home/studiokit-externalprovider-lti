﻿using Newtonsoft.Json;

namespace StudioKit.ExternalProvider.Lti.BusinessLogic.Models
{
	public class NamesRoleService
	{
		[JsonProperty("context_memberships_url")]
		public string ContextMembershipsUrl { get; set; }

		[JsonProperty("service_version")]
		public string ServiceVersion { get; set; }
	}
}