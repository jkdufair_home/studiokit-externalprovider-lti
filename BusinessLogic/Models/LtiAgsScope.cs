﻿namespace StudioKit.ExternalProvider.Lti.BusinessLogic.Models
{
	public static class LtiAgsScope
	{
		public static string LineItem = "https://purl.imsglobal.org/spec/lti-ags/scope/lineitem";

		public static string ResultReadOnly = "https://purl.imsglobal.org/spec/lti-ags/scope/result.readonly";

		public static string Score = "https://purl.imsglobal.org/spec/lti-ags/scope/score";
	}
}