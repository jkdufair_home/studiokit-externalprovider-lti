﻿namespace StudioKit.ExternalProvider.Lti.BusinessLogic.Models
{
	public static class LtiNrpsScope
	{
		public static string ContextMembershipsReadOnly = "https://purl.imsglobal.org/spec/lti-nrps/scope/contextmemberships.readonly";
	}
}