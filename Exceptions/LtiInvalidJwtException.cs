﻿using System;

namespace StudioKit.ExternalProvider.Lti.Exceptions
{
	public class LtiInvalidJwtException : Exception
	{
		public LtiInvalidJwtException()
		{
		}

		public LtiInvalidJwtException(string message) : base(message)
		{
		}

		public LtiInvalidJwtException(string message, Exception inner) : base(message, inner)
		{
		}
	}
}