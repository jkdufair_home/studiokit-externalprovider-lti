﻿using StudioKit.Data.Entity.Identity.Models;
using StudioKit.ExternalProvider.Lti.Models;
using StudioKit.ExternalProvider.Models.Interfaces;
using System.Data.Entity;
using System.Threading;
using System.Threading.Tasks;

namespace StudioKit.ExternalProvider.Lti.DataAccess
{
	public interface ILtiLaunchDbContext<TGroupUserRole, TGroupUserRoleLog, TExternalGroup, TExternalGroupUser>
		where TGroupUserRole : class, IGroupUserRole, new()
		where TGroupUserRoleLog : class, IGroupUserRoleLog, new()
		where TExternalGroup : class, IExternalGroup, new()
		where TExternalGroupUser : class, IExternalGroupUser<TExternalGroup>, new()
	{
		Task<int> SaveChangesAsync(CancellationToken cancellationToken = default(CancellationToken));

		#region DbSets

		DbSet<LtiExternalProvider> LtiExternalProviders { get; set; }

		IDbSet<Role> Roles { get; set; }

		DbSet<TExternalGroup> ExternalGroups { get; set; }

		DbSet<TExternalGroupUser> ExternalGroupUsers { get; set; }

		DbSet<TGroupUserRole> GroupUserRoles { get; set; }

		DbSet<TGroupUserRoleLog> GroupUserRoleLogs { get; set; }

		DbSet<LtiLaunch> LtiLaunches { get; set; }

		#endregion DbSets
	}
}