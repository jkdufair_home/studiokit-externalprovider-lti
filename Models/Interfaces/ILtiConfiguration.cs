﻿using System;

namespace StudioKit.ExternalProvider.Lti.Models.Interfaces
{
	public interface ILtiConfiguration
	{
		byte[] PrivateRsaKey { get; set; }

		byte[] PublicRsaKey { get; set; }

		DateTime DateLastUpdated { get; set; }
	}
}