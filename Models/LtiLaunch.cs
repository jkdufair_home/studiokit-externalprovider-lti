﻿using StudioKit.Data;
using StudioKit.Data.Entity.Identity.Interfaces;
using StudioKit.Data.Interfaces;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace StudioKit.ExternalProvider.Lti.Models
{
	public class LtiLaunch : ModelBase, IOwnable
	{
		[Required]
		public int ExternalProviderId { get; set; }

		[ForeignKey(nameof(ExternalProviderId))]
		public virtual LtiExternalProvider ExternalProvider { get; set; }

		[Required]
		public string CreatedById { get; set; }

		[ForeignKey(nameof(CreatedById))]
		public virtual IUser User { get; set; }

		[Required]
		public string ExternalId { get; set; }

		[Required]
		public string Name { get; set; }

		public string Description { get; set; }

		public string Roles { get; set; }

		public string RosterUrl { get; set; }

		public string LtiVersion { get; set; }

		public string ReturnUrl { get; set; }

		public string GradesUrl { get; set; }
	}
}